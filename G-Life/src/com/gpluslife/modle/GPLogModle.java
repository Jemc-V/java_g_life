package com.gpluslife.modle;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class GPLogModle {

	public String ID = "_id";
	public String APP_ID = "app_id";
	public String APPNAME = "app_name";
	public String CREATETIME = "time";
	public String CONTENT = "content";
	public String ISNOTE = "isnote";
	public String LOGID = "logid";
	public String PICTURES = "pictures";
	public String MODULE = "module";

	public GPLogModle() {

	}

	public GPLogModle(String appId, String appName, String creatTime,
			String content, String isNote, String logId, String pics,
			String module) {
		this.APP_ID = appId;
		this.APPNAME = appName;
		this.CREATETIME = creatTime;
		this.CONTENT = content;
		this.ISNOTE = isNote;
		this.LOGID = logId;
		this.PICTURES = pics;
		this.MODULE = module;
	}

	public static List<GPLogModle> initModel(JSONObject jsonObj)
			throws JSONException {
		List<GPLogModle> models = new ArrayList<GPLogModle>();
		JSONObject resultObject = jsonObj.getJSONObject("result");
		JSONArray result = resultObject.getJSONArray("result");
		Log.d("model", result.toString());
		for (int i = 0; i < result.length(); i++) {
			JSONObject jsonModel = result.getJSONObject(i);
			Log.d("model", jsonModel.toString());
			Boolean flag = jsonModel.getBoolean("isNote");
			String logId = jsonModel.getString("logId");
			String content = jsonModel.getString("content");
			String isNote = String.valueOf(flag);
			String createTime = jsonModel.getString("createTime");
			String pic = "";
			Boolean isEmpty = jsonModel.has("pictures");
			String appname = "";
			String appid = "";
			String module = "";
			if (isEmpty) {
				JSONArray jsonArray = jsonModel.getJSONArray("pictures");
				pic = jsonArray.toString();
			}
			if (!flag) {
				appname = jsonModel.getString("appName");
				appid = jsonModel.getString("appId");
				module = jsonModel.getString("module");
			}
			GPLogModle model = new GPLogModle(appid, appname, createTime, content,
					isNote, logId, pic, module);
			models.add(model);
		}
		// Log.d("LogModle", "model="+model);
		return models;
	}

}
