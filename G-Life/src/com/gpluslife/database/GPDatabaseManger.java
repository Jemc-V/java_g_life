package com.gpluslife.database;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.gpluslife.modle.GPLogModle;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;



public class GPDatabaseManger {

	public static List<GPLogModle> getLogDataBy(Context context, int page) {
		String phone = null;
		SQLiteDatabase sqlitedb;
		GPDatabaseActivity dbhelp;
		ArrayList<GPLogModle> listsports = null;
		
		 dbhelp = new GPDatabaseActivity(context);
		sqlitedb = dbhelp.getWritableDatabase();

		int offset = page * 20;
		String where = GPDatabaseActivity.CREATETIME + " DESC limit 20 offset " + offset;
		Cursor cursor = sqlitedb.query(GPDatabaseActivity.TABLE_NAME, null, null, null,
				null, null, where);
		
	
		
		if (cursor != null && cursor.getCount() != 0) {
			listsports = new ArrayList<GPLogModle>();
			while (cursor.moveToNext()) {
				GPLogModle smm = new GPLogModle();
				smm.APP_ID = cursor.getString(cursor
						.getColumnIndex(GPDatabaseActivity.APP_ID));//
				smm.APPNAME = cursor.getString(cursor
						.getColumnIndex(GPDatabaseActivity.APPNAME));//
				smm.CONTENT = cursor.getString(cursor
						.getColumnIndex(GPDatabaseActivity.CONTENT));//
				smm.CREATETIME = cursor.getString(cursor
						.getColumnIndex(GPDatabaseActivity.CREATETIME));//
				smm.ISNOTE = cursor.getString(cursor
						.getColumnIndex(GPDatabaseActivity.ISNOTE));//
				smm.LOGID = cursor.getString(cursor
						.getColumnIndex(GPDatabaseActivity.LOGID));//
				smm.MODULE = cursor.getString(cursor
						.getColumnIndex(GPDatabaseActivity.MODULE));//
				smm.PICTURES = cursor.getString(cursor
						.getColumnIndex(GPDatabaseActivity.PICTURES));//
				listsports.add(smm);
			}
		}
		if (cursor != null)
			cursor.close();
//		System.out.println("获取数据库数据的长度listsports.size()"+listsports.size());
		return listsports;

	}

	// 获取某天的某种运动数据
	public static List<GPLogModle> getLogData(Context context, int page) {
		return getLogDataBy(context, page);
	}

}
