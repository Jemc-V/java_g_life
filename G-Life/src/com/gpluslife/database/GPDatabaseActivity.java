package com.gpluslife.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.gpluslife.filemanager.GPFileManager;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

public class GPDatabaseActivity extends SQLiteOpenHelper{
	
	public static String DATABASE_NAME = "data.db";
	public static final String TABLE_NAME = "G_LifeDataBase";
	public static final String ID = "_id";
	public static final String APP_ID = "app_id";
	public static final String APPNAME = "app_name";
	public static final String CREATETIME = "time";
	public static final String CONTENT = "content";
	public static final String ISNOTE = "isnote";
	public static final String LOGID = "logid";
	public static final String PICTURES = "pictures";
	public static final String MODULE = "module";
	public static int DATABASE_VERSION = 1 ;
	
	
	public static	final String Sql =  "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
	+ " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + APP_ID
	+ " TEXT," + APPNAME + " TEXT," + CONTENT + " TEXT," + CREATETIME
	+ " TEXT," + ISNOTE + " TEXT," + LOGID + " TEXT," + MODULE
	+ " TEXT," + PICTURES + " TEXT" + " )";

	public GPDatabaseActivity(Context context) {
		super(new GPLogDatabase(context), DATABASE_NAME, null, DATABASE_VERSION);
		// TODO 自动生成的构造函数存根
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO 自动生成的方法存根
		System.out.println("GPDatabaseActivity.java =====>>>开始创建数据库表");
		try{
			db.execSQL(Sql);
			System.out.println("GPDatabaseActivity.java =====>>>创建数据库表成功");
		}catch (SQLException e){
			e.printStackTrace();
			System.out.println("GPDatabaseActivity.java =====>>>创建数据库表失败");
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO 自动生成的方法存根
		 //db.execSQL("ALTER TABLE person ADD COLUMN other STRING"); 
	}

}
