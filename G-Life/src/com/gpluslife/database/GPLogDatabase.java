package com.gpluslife.database;

import java.io.File;
import java.io.IOException;

import com.gpluslife.filemanager.GPFileManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

public class GPLogDatabase extends ContextWrapper {

	public GPLogDatabase(Context base) {
		super(base);

	}

	/**
	 * 获得数据库路径，如果不存在，则创建对象对象
	 */
	@Override
	public File getDatabasePath(String name) {
		String dbname = "/database/data.db";
		String[] files = dbname.split(File.separator);
		String nameString = files[files.length - 1];
		String tempNameString = dbname.replace(nameString, "");
		String path = GPFileManager.getFileManager().createFilePath(
				tempNameString);
		path += nameString;

		File file = new File(path);
		if (file.exists() == false) {
			try {
				Boolean success = file.createNewFile();
				if (success) {
					Log.d("", "123456789");
				}
			} catch (IOException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
		return file;
	}

	/**
	 * 重载这个方法，是用来打开SD卡上的数据库的，android 2.3及以下会调用这个方法。
	 */
	@Override
	public SQLiteDatabase openOrCreateDatabase(String name, int mode,
			CursorFactory factory) {
		return SQLiteDatabase.openOrCreateDatabase(getDatabasePath(name),
				factory);
	}

	/**
	 * Android 4.0会调用此方法获取数据库。
	 */
	@SuppressLint("NewApi")
	@Override
	public SQLiteDatabase openOrCreateDatabase(String name, int mode,
			CursorFactory factory, DatabaseErrorHandler errorHandler) {
		return SQLiteDatabase.openOrCreateDatabase(getDatabasePath(name)
				.getAbsolutePath(), factory, errorHandler);
	}
}
