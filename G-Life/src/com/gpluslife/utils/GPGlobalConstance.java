package com.gpluslife.utils;

import android.net.Uri;

public class GPGlobalConstance {
	public static final int GROBAL_BLACELET_TIME_OUT  = 5000;
	public static final String GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED  = "com.glife.jordan";
	public static final Uri URI_DEVICE_ADD_REMOVE_BALCELET = Uri.parse("content://"+GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED);

	public static final String PREFERENCE_KEY_PHONE_NOTIFY = "com.gplus.health.blacelet.phone.notify";
	public static final String PREFERENCE_KEY_MESSAGE_NOTIFY = "com.gplus.health.blacelet.message.notify";
}
