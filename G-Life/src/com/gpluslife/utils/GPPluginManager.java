package com.gpluslife.utils;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;


public class GPPluginManager {
	/**
	 * 
	 * @param rootView
	 * @param layoutName: the name of view's layout
	 */
	public static void addPluginViews(LinearLayout rootView, String layoutName, String packageName){
		Context context =  rootView.getContext();
		try {
			Context marsterContext = getTargetContext(context, packageName);
			 View view = getView(marsterContext, getId(marsterContext, "layout", layoutName));
			 rootView.addView(view);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	 public static View getView(Context targetContext, int id) {
	        return ((LayoutInflater) targetContext
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(id,
	                null);
	    }
	 
	 /**
	  * 
	  * @param targetContext the context of the target res
	  * @param resType:"layout" or "String","drawable" and so on
	  * @param resName:the name of "layout" or "String" and so on
	  * @return the ID of the res specified by resType and name 0 not found
	  */
	 public static int getId(Context targetContext, String resType, String resName) {
	        return targetContext.getResources().getIdentifier(resName, resType, 
	        		targetContext.getPackageName());
	    }
	
	 /**
	  * 
	  * @param context current context
	  * @param packageName the package name of target context
	  * @return
	  * @throws NameNotFoundException
	  */
	public static Context getTargetContext(Context context, String packageName) throws NameNotFoundException {
        return context.createPackageContext(packageName,
                Context.CONTEXT_IGNORE_SECURITY | Context.CONTEXT_INCLUDE_CODE); 
    }
	
}
