package com.gpluslife.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

public class GPGetPulgeJson {
	private Activity activity;
	
	public GPGetPulgeJson(Activity activity) {
		this.activity = activity;
	}
	
	public StringBuffer getData() {
		StringBuffer json = new StringBuffer();
		try {
			InputStreamReader isr = new InputStreamReader(activity.getResources().getAssets().open("plugin.json"));
			BufferedReader br = new BufferedReader(isr);
			char[] tempchars  = new char[1024];
			int charread = 0;
			while ((charread = br.read(tempchars)) != -1) {
				json.append(tempchars);
			}
			Log.v("GetAddressUtil","json数据"+ json);
			return json;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setPulgins() {
		List<String> provinceList = new ArrayList<String>();
		
		String json = getData().toString();
		try {
			JSONObject data =new JSONObject(json);
			JSONArray citycodes = data.getJSONArray("cityCode");
			for(int i=0;i<citycodes.length();i++){
				JSONObject citycCode = citycodes.getJSONObject(i);
				System.out.println("JSON test:::Json Object is:::" + citycCode);
				String province = citycCode.getString("province");
				provinceList.add(province);
				JSONArray citys = citycCode.getJSONArray("cities");
				List<String> cityList = new ArrayList<String>();
				for(int j=0;j<citys.length();j++){
					JSONObject citycode = citys.getJSONObject(j);
					String city = citycode.getString("city");
					String code = citycode.getString("code");
					cityList.add(city);
				
				}
				
			}
			
		} catch (JSONException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}	
	}	
	
	
	public void setPlugin(){
		String json = getData().toString();
		try {
			JSONArray dataArray = new JSONArray(json);
			for (int i = 0 ; i < dataArray.length();i++){
				JSONObject plugins = dataArray.getJSONObject(i);
				String pluginId = plugins.getString("pluginId");
				String plugin = plugins.getString("plugin");
				String pluginThumb = plugins.getString("pluginThumb");
				String pluginSetting = plugins.getString("pluginSetting");
				String isCreateLog = plugins.getString("isCreateLog");
		
			}
		} catch (JSONException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
	}
	
}
