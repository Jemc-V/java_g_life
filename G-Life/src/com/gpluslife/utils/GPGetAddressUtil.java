package com.gpluslife.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

public class GPGetAddressUtil {
	private HashMap<String, List<String>> cityMap = new HashMap<String, List<String>>();
	private HashMap<String, String> codeMap = new HashMap<String, String>();
	private Activity activity;
	
	public GPGetAddressUtil(Activity activity) {
		this.activity = activity;
	}

	public StringBuffer getData() {
		StringBuffer json = new StringBuffer();
		try {
			InputStreamReader isr = new InputStreamReader(activity.getResources().getAssets().open("city1.json"));
			BufferedReader br = new BufferedReader(isr);
			char[] tempchars  = new char[1024];
			int charread = 0;
			while ((charread = br.read(tempchars)) != -1) {
				json.append(tempchars);
			}
			Log.v("GetAddressUtil","json数据"+ json);
			return json;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<String> getProvinceList() {
		List<String> provinceList = new ArrayList<String>();
		
		String json = getData().toString();
		try {
			JSONObject data =new JSONObject(json);
			JSONArray citycodes = data.getJSONArray("cityCode");
			for(int i=0;i<citycodes.length();i++){
				JSONObject citycCode = citycodes.getJSONObject(i);
				System.out.println("JSON test:::Json Object is:::" + citycCode);
				String province = citycCode.getString("province");
				provinceList.add(province);
				JSONArray citys = citycCode.getJSONArray("cities");
				List<String> cityList = new ArrayList<String>();
				for(int j=0;j<citys.length();j++){
					JSONObject citycode = citys.getJSONObject(j);
					String city = citycode.getString("city");
					String code = citycode.getString("code");
					cityList.add(city);
					codeMap.put(city, code);
				}
				cityMap.put(province, cityList);
			}
			
		} catch (JSONException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		
		
		
		/*try {
			JSONArray data = new JSONArray(json);
			for (int i = 0; i < data.length(); i++) {
				JSONObject provinceObj = data.getJSONObject(i);
				String province = provinceObj.getString("name");
				provinceList.add(province);
				JSONArray citys = provinceObj.getJSONArray("citys");
				List<String> cityList = new ArrayList<String>();
				for (int j = 0; j < citys.length(); j++) {
					JSONObject cityObj = citys.getJSONObject(j);
					Iterator<String> keys = cityObj.keys();
					 while(keys.hasNext()){
						 String key = keys.next();
						 String city = cityObj.getString(key);
						 cityList.add(city);
						 codeMap.put(city, key);
					 }
					 if (j == citys.length()-1) {
						 cityMap.put(province, cityList);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}*/
		return provinceList;
	}

	
	public List<String> getCityList(String province) {
		
		return cityMap.get(province);
	}
	
	
	public String getCity(String code){
		getProvinceList() ;
		String city = (String)keyString(codeMap, code);
		return city;
	}
	
	
	public String getProvince(String city){
			Iterator<String> it= cityMap.keySet().iterator();
	          while(it.hasNext()){
	        	  String province = it.next();
	        	  List<String> cityList = cityMap.get(province);
	        	  for (int i = 0; i < cityList.size(); i++) {
	        		  if (cityList.get(i).equals(city)) {
						return province;
					}
				}
	          }
	          return null;
	}
	
	
	public String getCode(String city){
		return codeMap.get(city);
	}
	
	public static Object keyString(HashMap<String,String> map,String o)
	{
	          Iterator<String> it= map.keySet().iterator();
	          while(it.hasNext())
	          {
	              Object keyString=it.next();
	              if(map.get(keyString).equals(o))
	                   return keyString;
	           }
	           return null;
	}
}
