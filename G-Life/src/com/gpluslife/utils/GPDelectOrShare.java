package com.gpluslife.utils;

import com.gpluslife.R;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;

public class GPDelectOrShare extends AlertDialog {
	public GPDelectOrShare(Context context, int theme) {
	    super(context, theme);
	}

	public GPDelectOrShare(Context context) {
	    super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.delectorshare);
	}
}
