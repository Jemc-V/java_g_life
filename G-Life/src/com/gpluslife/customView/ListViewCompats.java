package com.gpluslife.customView;



//import com.gpluslife.activity.Family_memberActivity.MessageItem;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

public class ListViewCompats extends ListView {

    private static final String TAG = "ListViewCompats";

    private GPFamilyMemberSlideView mFocusedItemView;
    private Context context;

    public ListViewCompats(Context ctx) {
        super(ctx);
    }

    public ListViewCompats(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ListViewCompats(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void shrinkListItem(int position) {
        View item = getChildAt(position);

        if (item != null) {
            try {
                ((GPFamilyMemberSlideView) item).shrink();
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN: {
            int x = (int) event.getX();
            int y = (int) event.getY();
            int position = pointToPosition(x, y);
            Log.e(TAG, "postion=" + position);
            if (position != INVALID_POSITION) {
//                MessageItem data = (MessageItem) getItemAtPosition(position);
//                mFocusedItemView = data.family_member_SlideView;
//                Log.e(TAG, "FocusedItemView=" + mFocusedItemView);
            }
        }
        default:
            break;
        }

        if (mFocusedItemView != null) {
            mFocusedItemView.onRequireTouchEvent(event);
        }

        return super.onTouchEvent(event);
    }

}
