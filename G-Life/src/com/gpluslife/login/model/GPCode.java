package com.gpluslife.login.model;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gpluslife.R;
import com.gpluslife.home.GPHomeActivity;
import com.gpluslife.service.GPUserService;
import com.gpluslife.service.http.GPClientCallBack;

public class GPCode extends Activity implements OnClickListener {

	private static final String TAG = "Code";
	private Button button;
	private ImageButton return_img;
	private EditText editText;
	private Button code_time;
	private TimeCount time;
	private int code;
	private String sessionId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_code);
		editText = (EditText) findViewById(R.id.code_edit);
		button = (Button) findViewById(R.id.OK_back);
		return_img = (ImageButton) findViewById(R.id.return_img);
		return_img.setOnClickListener(this);
		button.setOnClickListener(this);

		code_time=(Button) findViewById(R.id.code_time);
		code_time.setOnClickListener(this);
		datatime();
		
	}

	private void datatime() {
		// TODO Auto-generated method stub
		time = new TimeCount(60000, 1000);
		time.start();// 开始计时
		code_time = (Button) findViewById(R.id.code_time);
	}

	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);// 参数依次为总时长,和计时的时间间隔
		}

		public void onFinish() {// 计时完毕时触发
			code_time.setText("重新获取验证码");
			code_time.setClickable(true);
		}

		public void onTick(long millisUntilFinished) {// 计时过程显示
			code_time.setClickable(false);
			code_time.setText(millisUntilFinished / 1000 + "秒重新获取验证码");
		}
	}
		@Override
		public void onClick(View v) {
			Intent intentq = getIntent();
			sessionId = intentq.getStringExtra("sessionId");
			switch (v.getId()) {
			case R.id.OK_back:
				String codelength = editText.getEditableText().toString();
				if (codelength.length() == 4) {
					int codes = Integer.parseInt(codelength);
					Intent intent = getIntent();
					final String phone = intent.getStringExtra("number");
					final long ph = Long.parseLong(phone);
					final String password = intent.getStringExtra("password");

					GPUserService.get().register(ph, password, codes,
							new GPClientCallBack() {
								@Override
								public void onSuccess(String result) {
									Gson json = new Gson();
									GPBaseHttpEnitity base = json.fromJson(
											result, GPBaseHttpEnitity.class);
									if (base != null) {
									    code = Integer.parseInt(String.valueOf(base.getCode()));
										System.out.println("验证码验证状态返回值="+ code);
										if (code == 200) {
											SharedPreferences sp = getSharedPreferences("sessionId",
													MODE_WORLD_READABLE);
											Editor editor = sp.edit();
											editor.putString("number", phone);
											editor.putString("password", password);
											editor.commit();
											Intent intent5 = new Intent();
											intent5.setClass(GPCode.this,
													GPHomeActivity.class);
											startActivity(intent5);
											finish();
										} 
									}
								}
//
							});
					if (code == 100003) {
						Toast.makeText(GPCode.this,"用户存在",Toast.LENGTH_LONG).show();
					} else if (code == 100004) {
						Toast.makeText(GPCode.this,"验证码错误",Toast.LENGTH_LONG).show();
					} else if (code == 100005) {
						Toast.makeText(GPCode.this,"验证码错误",Toast.LENGTH_LONG).show();
					}else if(code<200){
						Toast.makeText(GPCode.this,"系统错误或者参数错误", Toast.LENGTH_LONG).show();
					}else {
					}
				}else {
					Toast.makeText(GPCode.this, "验证码格式错误！", 3000).show();
				}
				break;
			case R.id.return_img:
				onBackPressed();
				finish();
				break;
			case R.id.code_time:
				datatime();
				codeinit();
				break;
			default:
				break;
			}
		}

		private void codeinit() {
			// TODO Auto-generated method stub
			//重新获取验证码
			SharedPreferences sp = getSharedPreferences("sessionId",
					MODE_WORLD_READABLE);
			String username="";
			String phone = sp.getString("username", username);
			GPUserService.get().getSMS(Long.parseLong(phone),
					new GPClientCallBack() {
				@Override
				public void onSuccess(String result) {
					Gson json = new Gson();
					GPBaseHttpEnitity base = json.fromJson(result,
							GPBaseHttpEnitity.class);
					if (base != null) {
						code = Integer.parseInt(String.valueOf(base.getCode()));
						System.out.println("注册状态返回值=" + code);
						if (code == 200) {
							Log.d(TAG, "注册。再次请求短信验证码返回值code="+code);
						}
					}
				}
			});
		}
}