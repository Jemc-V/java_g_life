package com.gpluslife.login.model;

import java.util.List;

import com.gpluslife.entity.GPFunctionEntity;

import android.R.integer;
import android.R.string;

public class GPLoginUserModel {

	private String nickname;
	private String userName;
	private String accountName;
	private String signature;

	private String password;
	private int gender;
	
	private String birthday;
	
	private String district;
	
	private String sessionId;

	private List<GPFunctionEntity> functions;
	
	private String iconMD5;
	
	private String qRCodeMD5;
	private String deviceKey;
	private Boolean	isDownloadIcon;
	private Boolean	isDownloadQRcode;
	
	
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public List<GPFunctionEntity> getFunctions() {
		return functions;
	}

	public void setFunctions(List<GPFunctionEntity> functions) {
		this.functions = functions;
	}

	public String getIconMD5() {
		return iconMD5;
	}

	public void setIconMD5(String iconMD5) {
		this.iconMD5 = iconMD5;
	}

	public String getqRCodeMD5() {
		return qRCodeMD5;
	}

	public void setqRCodeMD5(String qRCodeMD5) {
		this.qRCodeMD5 = qRCodeMD5;
	}

	public String getDeviceKey() {
		return deviceKey;
	}

	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}

	public Boolean getIsDownloadIcon() {
		return isDownloadIcon;
	}

	public void setIsDownloadIcon(Boolean isDownloadIcon) {
		this.isDownloadIcon = isDownloadIcon;
	}

	public Boolean getIsDownloadQRcode() {
		return isDownloadQRcode;
	}

	public void setIsDownloadQRcode(Boolean isDownloadQRcode) {
		this.isDownloadQRcode = isDownloadQRcode;
	}
	
	public static void setModel(GPLoginUserModel model) {
		UserHold.loginUser = model;
	}

	public static GPLoginUserModel get()
	{
		return UserHold.loginUser;
	}
	
	public static class UserHold
	{
		private static GPLoginUserModel loginUser = new GPLoginUserModel();
	}
		
}
