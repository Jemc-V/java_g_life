package com.gpluslife.login;

import java.util.Map;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gpluslife.R;
import com.gpluslife.database.GPDatabaseActivity;
import com.gpluslife.database.GPLogDatabase;
import com.gpluslife.home.GPHomeActivity;
import com.gpluslife.home.plugin.management.GPPluginJSONParser;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.login.model.GPUserModelManager;
import com.gpluslife.login.register.GPRegisterActivity;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPErrorStlye;
import com.gpluslife.utils.GPUtils;

public class GPLoginActivity extends Activity implements OnClickListener {
	
	private static final int REQUEST_CODE=1;
	private EditText usernameText;
	private EditText passwordText;
	private TextView registerText;
	private TextView lostpassworkTextView;
	private static final String TAG = "GPLoginActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		GPPluginJSONParser.parserJsonString(GPLoginActivity.this);
		
		Button logiButton = (Button)findViewById(R.id.login);
		registerText = (TextView)findViewById(R.id.reg);
		lostpassworkTextView = (TextView)findViewById(R.id.los);
		usernameText = (EditText)findViewById(R.id.user_edit);
		passwordText = (EditText)findViewById(R.id.pwad_edit);
		usernameText.setText("13924659201");
		passwordText.setText("123456");
		registerText.setOnClickListener(this);
		lostpassworkTextView.setOnClickListener(this);
		logiButton.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int viewId = v.getId();
		switch (viewId) {
		case R.id.login:
		{

			final String usernameString = usernameText.getText().toString();
			final String passwordString = passwordText.getText().toString();
			
			Log.d(TAG, "usernameString == > " + usernameString  + "   passwordText==> "+ passwordString);
			
			if(usernameString != null && passwordString != null)
			{
				final GPLoginLoadingDialog loadingDialog = new GPLoginLoadingDialog(GPLoginActivity.this);
				loadingDialog.show();
				String deviceKey = Secure.getString(
						GPLoginActivity.this.getContentResolver(),
						Secure.ANDROID_ID);
				GPRequestService.get().login(usernameString, passwordString,
						deviceKey, new GPClientCallBack() {
							public void onSuccess(String result)
									throws Exception {
								
								Log.d(TAG, "login result == > " +result);
								
								Map<String, Object> map = GPUtils
										.getMapForJson(result);
								
								int code = 0;
								if (map.containsKey("code")) 
								{
									code = Integer.parseInt(map.get("code")
											.toString());
								}
								if (code == 200)
								{
						
								
									if (map.containsKey("result")) 
									{								
										String resultString = map.get("result")
												.toString();
										GPLoginUserModel userModel = GPUserModelManager
												.parserJsonString(GPLoginActivity.this,resultString,usernameString,true);

										GPUserModelManager
												.saveUserModelToSharePreferences(
														GPLoginActivity.this,
														userModel,
														passwordString);
										loadingDialog.dismiss();
										Intent intent = new Intent();
										intent.setClass(GPLoginActivity.this,
												GPHomeActivity.class);
										startActivity(intent);
										
										Looper.prepare();
										Toast.makeText(GPLoginActivity.this, "登录成功！", Toast.LENGTH_SHORT).show();
										Looper.loop();
							
									} 
								}
								else {
									loadingDialog.dismiss();
									GPErrorStlye.stlye(
											GPLoginActivity.this, code);

								}
							}
						});
			}
			
			
		}
			break;

		case R.id.reg:
			Intent intentReg = new Intent(GPLoginActivity.this,GPRegisterActivity.class);
			startActivity(intentReg);
			break;
			
		case R.id.los:
			Intent intentLos = new Intent(GPLoginActivity.this,GPLospasswaActivity.class);
			startActivity(intentLos);
			break;
			
		default:
			break;
		}
		
	}

}
