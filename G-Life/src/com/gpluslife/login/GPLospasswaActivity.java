package com.gpluslife.login;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gpluslife.R;
import com.gpluslife.login.model.GPBaseHttpEnitity;
import com.gpluslife.service.GPUserService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPUtils;

public class GPLospasswaActivity extends Activity implements OnClickListener{

	private static final String TAG = "Lospasswad";
	private EditText number,password,code;
	private TextView set_return;
	private Button ok;
    private TextView code_time;
	private TimeCount time;
	private String sessionId;
	private String iconMD5;
	int getcode;
	@SuppressLint("CutPasteId")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lospasswa);
		number=(EditText) findViewById(R.id.number);
		password=(EditText) findViewById(R.id.password);
		code=(EditText) findViewById(R.id.code);
		code_time=(TextView) findViewById(R.id.los_code_time);
		ok=(Button) findViewById(R.id.ok); 
		code_time.setOnClickListener(this);
		ok.setOnClickListener(this);
		set_return=(TextView) findViewById(R.id.set_return);
		set_return.setOnClickListener(this);
		
	}
	
	private void datatime() {
		// TODO Auto-generated method stub
		time = new TimeCount(60000, 1000);
		time.start();// 
	}

	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);// 
		}

		public void onFinish() {// 
			code_time.setClickable(true);
			code_time.setText("重新获取验证码");
		}

		public void onTick(long millisUntilFinished) {// 
			code_time.setClickable(false);
			code_time.setText(millisUntilFinished / 1000 + "秒后重新获取验证码");
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.los_code_time:
			init();
			break;
		case R.id.ok:
			codeinit();
			break;
		case R.id.set_return:
			onBackPressed();
			finish();
			break;
		default:
			break;
		}
	}
	private void codeinit() {
		// TODO Auto-generated method stub
		//
		final String pnumber = number.getText().toString();
		final String passoword=password.getText().toString();
		final String sendcode=code.getEditableText().toString();
		Log.d(TAG, "忘记密码参数"+"pnumber="+pnumber+"/passoword="+passoword+"/sendcode="+sendcode);
		if(sendcode.length() ==0){
			Toast.makeText(this, "请输入验证码！", Toast.LENGTH_SHORT).show();
		}else{
		GPUserService.get().reset(Integer.parseInt(sendcode), Long.parseLong(pnumber), passoword, new GPClientCallBack() {
			@Override
			public void onSuccess(String jsonString) throws Exception {
				Map<String, Object> map = GPUtils.getMapForJson(jsonString);
				int code2 = Integer.parseInt(map.get("code").toString());
				if (code2 == 200) {
					Log.d(TAG, "忘记密码成功");
					Intent  intent=new Intent().setClass(GPLospasswaActivity.this, GPLoginActivity.class);
					startActivity(intent);
					finish();
				}
			}
		});
		}
	}
	private void init() {
		// TODO Auto-generated method stub
		//
		    String sendcode=code.getEditableText().toString();
			String pnumber = number.getText().toString();
			String check = "^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
			Pattern p = Pattern.compile(check);
			Matcher m = p.matcher(pnumber);
			if (!m.matches()) {
				Toast.makeText(this, "手机号码格式错误！", Toast.LENGTH_SHORT).show();
				return;
			}
		    if (pnumber.length() < 6) {
				Toast.makeText(this, "请输入6-18位的密码！", Toast.LENGTH_SHORT).show();
			}
		    if (sendcode.length() ==0) {
				Toast.makeText(this, "请输入验证码！", Toast.LENGTH_SHORT).show();
			}else {
				datatime();
				Log.d(TAG, "手机号码="+Long.parseLong(pnumber));
			GPUserService.get().getSMS(Long.parseLong(pnumber),
						new GPClientCallBack() {
							@Override
							public void onSuccess(String result) {
								Gson json = new Gson();
								GPBaseHttpEnitity base = json.fromJson(result,
										GPBaseHttpEnitity.class);
								if (base != null) {
									getcode = Integer.parseInt(String.valueOf(base
											.getCode()));
									System.out.println("获取短信code返回值=" + getcode);
									if(getcode == 200){
										Log.d(TAG, "获取短信成功");
									}
								}
							}
						});
			}
	}
}
