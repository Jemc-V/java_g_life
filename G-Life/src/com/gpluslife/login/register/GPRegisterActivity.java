package com.gpluslife.login.register;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.gpluslife.R;
import com.gpluslife.login.model.GPBaseHttpEnitity;
import com.gpluslife.login.model.GPCode;
import com.gpluslife.service.GPUserService;
import com.gpluslife.service.http.GPBaseCallBack;
import com.gpluslife.service.http.GPClientCallBack;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class GPRegisterActivity extends Activity implements OnClickListener {

	private static final String TAG = "UseRegister";
	private EditText num, pwd, pwd_ok;
	private Button button;
	private ImageButton return_btn;
	private int code;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		num = (EditText) findViewById(R.id.Phone_number_edit);
		pwd = (EditText) findViewById(R.id.pawd_edit);
		pwd_ok = (EditText) findViewById(R.id.pawd_edit_ok);
		return_btn = (ImageButton) findViewById(R.id.return_set);
		return_btn.setOnClickListener(this);
		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(this);

		// EditText tv1,tv2,tv3;
		// tv1=(EditText) findViewById(R.id.Phone_number_edit);
		// tv2=(EditText) findViewById(R.id.pawd_edit);
		// tv3=(EditText) findViewById(R.id.pawd_edit_ok);
		// Typeface face = Typeface.createFromAsset (getAssets() , "fz.TTF" );
		// tv1.setTypeface (face);
		// tv2.setTypeface (face);
		// tv3.setTypeface (face);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.button1) {
			nextFragment();
			if (code == 100003) {
				Toast.makeText(GPRegisterActivity.this, "用户存在,请重新输入！",
						Toast.LENGTH_SHORT).show();
			}
		}
		if (id == R.id.return_set) {
			onBackPressed();
			finish();
		}
	}

	public void nextFragment() {
		String pnumber = num.getEditableText().toString();
		String check = "^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
		Pattern p = Pattern.compile(check);
		Matcher m = p.matcher(pnumber);
		if (!m.matches()) {
			Toast.makeText(this, "手机格式错误！", Toast.LENGTH_SHORT).show();
			return;
		}
		final String phone = num.getEditableText().toString();
		final String username = pwd.getEditableText().toString();
		final String password = pwd_ok.getEditableText().toString();
		if (!username.equals(password)) {
			Toast.makeText(this, "请确认两次密码输入一致！", Toast.LENGTH_SHORT).show();
			Log.d(TAG, "请输入密码");
		} else if (username.length() < 6) {
			Toast.makeText(this, "请输入不小于6位数的密码！", Toast.LENGTH_SHORT).show();
		} else {
			GPUserService.get().getSMS(Long.parseLong(phone),
					new GPClientCallBack() {
						@Override
						public void onSuccess(String result) {
							Gson json = new Gson();
							GPBaseHttpEnitity base = json.fromJson(result,
									GPBaseHttpEnitity.class);
							if (base != null) {
								code = Integer.parseInt(String.valueOf(base
										.getCode()));
								System.out.println("注册状态返回值=" + code);
								if (code == 200) {
									Intent intent = new Intent();
									intent.putExtra("number", phone);
									intent.putExtra("password", password);
									intent.setClass(GPRegisterActivity.this,
											GPCode.class);
									startActivity(intent);
									finish();
								}
							}
						}

	
					});
		}

	}
}
