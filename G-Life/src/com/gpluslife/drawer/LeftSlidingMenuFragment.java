package com.gpluslife.drawer;

import java.io.File;
import java.util.Map;

import com.gpluslife.R;
//import com.gpluslife.fragment.AboutFragment;
//import com.gpluslife.fragment.Fragment_home;
//import com.gpluslife.fragment.FriendFragment;
//import com.gpluslife.fragment.ImageFragment;
//import com.gpluslife.fragment.MessageFragment;
//import com.gpluslife.fragment.RecentdevelopmentsFragment;
//import com.gpluslife.fragment.ShoppingFragment;
import com.gpluslife.home.GPHomeActivity;
//import com.gpluslife.http.ClientCallBack;
//import com.gpluslife.service.UserService;
//import com.gpluslife.utils.Utils;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class LeftSlidingMenuFragment extends Fragment implements
		OnClickListener {
	private static final String TAG = "LeftSlidingMenuFragment";
	GPHomeActivity appContext;
	GPHomeActivity activity;
	ViewPager vpager;
	LinearLayout relayout = null;
	private String filepath = "/mnt/sdcard/Gplus", filepathimg = "", nickname,
			signature;
	private FrameLayout mFrameLayout = null;
	private TableRow tab0, tab1, tab2, tab3, tab4, tab5, tab6, tab7;
	private ImageView logoimg;
	private TextView user_signature, user_name;
	Bitmap bitmap = null;
	String icon;
	String accountName, password;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// appContext = (MainActivity)getActivity().getApplicationContext();

	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.left_nav_menu_improve, container,
				false);
//		logoimg = (ImageView) view.findViewById(R.id.logoimg);
//		logoimg.setOnClickListener(this);
//		user_signature = (TextView) view.findViewById(R.id.user_signature);
//		user_name = (TextView) view.findViewById(R.id.user_name);
		// ======================================================
		filepathimg = filepath + "/" + "Gjname" + ".jpg";
		File f = new File(filepathimg);
		Log.d(TAG, "filepathimg构建成功");
		if (f.exists()) {
			Log.d(TAG, "存在文件");
			Bitmap bitmap = BitmapFactory.decodeFile(filepathimg);
			Drawable drawable = new BitmapDrawable(bitmap);
			logoimg.setBackgroundDrawable(drawable);
			Log.d(TAG, "抽屉设置头像成功");
		}
		// ========================================================
		SharedPreferences sp = getActivity().getSharedPreferences("sessionId",
				getActivity().MODE_WORLD_READABLE);
		nickname = sp.getString("nickname", nickname);
		signature = sp.getString("signature", signature);
		user_name.setText(nickname);
		user_signature.setText(signature);
		// ================================
//		activity = (MainActivity)getActivity();

		if (savedInstanceState == null) {
//			new Fragment_home();
		}
		// ================================================//

		// ================================================//

		// tab0 = (TableRow) view.findViewById(R.id.line_122);
		// tab0.setOnClickListener(this);
//		tab1 = (TableRow) view.findViewById(R.id.line_0);
//		tab1.setOnClickListener(this);
//		tab2 = (TableRow) view.findViewById(R.id.line_1);
//		tab2.setOnClickListener(this);
//		tab3 = (TableRow) view.findViewById(R.id.line_2);
//		tab3.setOnClickListener(this);
//		tab4 = (TableRow) view.findViewById(R.id.line_3);
//		tab4.setOnClickListener(this);
//		tab5 = (TableRow) view.findViewById(R.id.line_4);
//		tab5.setOnClickListener(this);
//		tab6 = (TableRow) view.findViewById(R.id.line_5);
//		tab6.setOnClickListener(this);
//		tab7 = (TableRow) view.findViewById(R.id.line_6);
//		tab7.setOnClickListener(this);

//		relayout = (LinearLayout) activity.findViewById(R.id.mainviewpager);
//		mFrameLayout = (FrameLayout) activity.findViewById(R.id.content_frame);
		return view;
	}

	@Override
	public void onClick(View v) {
		Fragment newContent = null;
		switch (v.getId()) {
		case R.id.logoimg:
			relayout.setVisibility(View.GONE);
			mFrameLayout.setVisibility(View.VISIBLE);
//			newContent = new ImageFragment();
			tab1.setSelected(false);
			tab2.setSelected(false);
			tab3.setSelected(false);
			tab4.setSelected(false);
			tab5.setSelected(false);
			tab6.setSelected(false);
			tab7.setSelected(false);
			Toast.makeText(getActivity(), "IMGE", Toast.LENGTH_SHORT).show();
			break;
		case R.id.line_0:
			relayout.setVisibility(View.VISIBLE);
			mFrameLayout.setVisibility(View.VISIBLE);
//			newContent = new Fragment_home();

			tab1.setSelected(true);
			tab2.setSelected(false);
			tab3.setSelected(false);
			tab4.setSelected(false);
			tab5.setSelected(false);
			tab6.setSelected(false);
			tab7.setSelected(false);
			Toast.makeText(getActivity(), "我", Toast.LENGTH_SHORT).show();
			break;
		case R.id.line_1:
			relayout.setVisibility(View.GONE);
			mFrameLayout.setVisibility(View.VISIBLE);
//			newContent = new FriendFragment();
			Toast.makeText(getActivity(), "好友", Toast.LENGTH_SHORT).show();
			tab1.setSelected(false);
			tab2.setSelected(true);
			tab3.setSelected(false);
			tab4.setSelected(false);
			tab5.setSelected(false);
			tab6.setSelected(false);
			tab7.setSelected(false);
			break;
		case R.id.line_2:
			relayout.setVisibility(View.GONE);
			mFrameLayout.setVisibility(View.VISIBLE);
//			newContent = new MessageFragment();
			Toast.makeText(getActivity(), "信息", Toast.LENGTH_SHORT).show();
			tab1.setSelected(false);
			tab2.setSelected(false);
			tab3.setSelected(true);
			tab4.setSelected(false);
			tab5.setSelected(false);
			tab6.setSelected(false);
			tab7.setSelected(false);
			break;
		case R.id.line_3:
			relayout.setVisibility(View.GONE);
			mFrameLayout.setVisibility(View.VISIBLE);
//			newContent = new RecentdevelopmentsFragment();
			tab1.setSelected(false);
			tab2.setSelected(false);
			tab3.setSelected(false);
			tab4.setSelected(true);
			tab5.setSelected(false);
			tab6.setSelected(false);
			tab7.setSelected(false);
			Toast.makeText(getActivity(), "近况", Toast.LENGTH_SHORT).show();
			break;
		case R.id.line_4:
			relayout.setVisibility(View.GONE);
			mFrameLayout.setVisibility(View.VISIBLE);
//			newContent = new ShoppingFragment();
			tab1.setSelected(false);
			tab2.setSelected(false);
			tab3.setSelected(false);
			tab4.setSelected(false);
			tab5.setSelected(true);
			tab6.setSelected(false);
			tab7.setSelected(false);
			Toast.makeText(getActivity(), "商城", Toast.LENGTH_SHORT).show();
			break;
		case R.id.line_5:
			relayout.setVisibility(View.GONE);
			mFrameLayout.setVisibility(View.VISIBLE);
//			newContent = new AboutFragment();
			tab1.setSelected(false);
			tab2.setSelected(false);
			tab3.setSelected(false);
			tab4.setSelected(false);
			tab5.setSelected(false);
			tab6.setSelected(true);
			tab7.setSelected(false);
			Toast.makeText(getActivity(), "关于我们", Toast.LENGTH_SHORT).show();
			break;
		case R.id.line_6:
			relayout.setVisibility(View.GONE);
			mFrameLayout.setVisibility(View.VISIBLE);

			tab1.setSelected(false);
			tab2.setSelected(false);
			tab3.setSelected(false);
			tab4.setSelected(false);
			tab5.setSelected(false);
			tab6.setSelected(false);
			tab7.setSelected(true);
			Toast.makeText(getActivity(), "退出程序", Toast.LENGTH_SHORT).show();
			ShowOutapp();
			break;

		default:
			break;
		}
		if (newContent != null) {
			switchFragment(newContent);
		}
	}

	private void ShowOutapp() {
		// TODO Auto-generated method stub
		final AlertDialog dlg = new AlertDialog.Builder(getActivity()).create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的.
		// 设置窗口的内容页面,shrew_exit_dialog.xml文件中定义view内容
//		window.setContentView(R.layout.out_app);
		// 为确认按钮添加事件,执行退出应用操作
		Button ok = (Button) window.findViewById(R.id.checkout_btn);
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				getActivity().finish();
				System.exit(0);
			}
		});
//		Button cancel = (Button) window.findViewById(R.id.checkout_no);
//		cancel.setOnClickListener(new View.OnClickListener() {
//			public void onClick(View v) {
//				dlg.cancel();
//			}
//		});
		WindowManager.LayoutParams params = dlg.getWindow()   
                .getAttributes();   
        params.width = params.MATCH_PARENT;   
        params.height = 700;   
        params.x = 500;   
        params.y = 600;    
        dlg.getWindow().setAttributes(params);  
	}

	private void switchFragment(Fragment fragment) {
		if (getActivity() == null)
			return;
//		activity.switchContent(fragment);
	}

}
