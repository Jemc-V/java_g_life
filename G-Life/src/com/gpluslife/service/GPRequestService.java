package com.gpluslife.service;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Map;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.util.Base64;
import android.util.Log;

import com.gpluslife.service.http.GPBaseCallBack;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.service.http.GPHttpClient;
import com.gpluslife.utils.GPSecretUtil;;

@SuppressLint("NewApi") public class GPRequestService extends GPBaseService {

	private static final String GPLUS_USER_LOGIN_METHOD 						= "user.login";
	private static final String GPLUS_USER_RESET_PASSWORD_METHOD  	= "user.reset.password";
	private static final String GPLUS_USER_SMS_REGISTER_METHOD 			= "sms.register";
	private static final String GPLUS_USER_CHECK_PHONE_METHOD 			= "user.check";
	private static final String GPLUS_USER_UPLOAD_ICON_METHOD 			= "user.upload.icon";
	private static final String GPLUS_USER_DOWNLOAD_ICON_METHOD 		= "user.download.icon";
	private static final String GPLUS_USER_DOWN_QRCODE_METHOD 		=  "user.download.qrcode";
	private static final String GPLUS_USER_GET_INFO_METHOD 					=  "user.get.info";
	private static final String GPLUS_USER_PHONE_REGISTER_METHOD 		=	"user.phone.register";
	private static final String GPLUS_USER_UPDATE_INFO_METHOD 			= "user.update.info";
	private static final String GPLUS_LOG_SHARE_NOTE_METHOD 				= "share.log.note";
	private static final String GPLUS_LOG_UPLOAD_PHOTO_METHOD 			= "share.upload.notePicture";
	private static final String GPLUS_LOG_DOWNLOAD_PHOTO_METHOD 	= "share.download.notePicture";
	private static final String GPLUS_LOG_DOWNLOAD_THUMB_METHOD 	="share.download.noteThumbnail";
	private static final String GPLUS_LOG_DELETE_THUMB_METHOD 			="share.delete.log";
	private static final String GPLUS_FIMILY_GET_MEMBER_METHOD 			= "family.get.member";
	private static final String GPLUS_FIMILY_ADD_MEMBER_METHOD 			= "family.add.member";
	private static final String GPLUS_FIMILY_DELETE_MEMBER_METHOD 		= "family.delete.member";
	/**
	 * 鍗曚緥
	 */
	public GPRequestService() {

	}

	public static GPRequestService get() 
	{
		return Holder.instance;
	}

	// 鍦ㄧ锟�锟斤拷琚紩鐢ㄦ椂琚姞锟�
	private static class Holder 
	{
		private static GPRequestService instance = new GPRequestService();
	}
	
	/**
	 * 登录
	 * 
	 * @param userName
	 * @param passoword
	 * @param callBack
	 */
	/*public void login(String userName, String passoword,String deviceKey, CallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.login");
		map.put("userName", userName);
		map.put("password", SecretUtil.getSHA1DigestString(passoword));
		map.put("deviceKey", deviceKey);
		//deviceKey 手机唯一码

		map = this.builderSign(map);

		// 寮傛璋冪敤
		ClientHttp.asynPost(map, url, callBack);
	}*/
	public void login(String userName, String password,String deviceKey, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();
		
		map.put("method", GPLUS_USER_LOGIN_METHOD);
		map.put("userName", userName);
		map.put("password", GPSecretUtil.getSHA1DigestString(password));
		map.put("deviceKey", deviceKey);
		//deviceKey 手机唯一码
		
		map = this.builderSign(map);
		
		Log.d("GPLoginActivity  ",  "map ====> " + map.toString());
		
		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);
	}
	
	public void switchAccountLogin(String userName, String password,String deviceKey, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();
		
		map.put("method", GPLUS_USER_LOGIN_METHOD);
		map.put("userName", userName);
		map.put("password", password);
		map.put("deviceKey", deviceKey);
		//deviceKey 手机唯一码
		
		map = this.builderSign(map);
		
		Log.d("GPLoginActivity  ",  "switchAccountLogin map ====> " + map.toString());
		
		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);
	}
	
	/*
	public void checklogin(String userName, String passoword, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.login");
		map.put("userName", userName);
		map.put("password", passoword);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}
	*/
	public void reset(int code, Long phone,String passoword, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_USER_RESET_PASSWORD_METHOD);
		map.put("code", String.valueOf(code));
		map.put("phone", String.valueOf(phone));
		map.put("passoword", GPSecretUtil.getSHA1DigestString(passoword));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 鑾峰彇鐭俊
	 * 
	 * @param phone
	 * @param callBack
	 */
	public void getSMS(long phone, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_USER_SMS_REGISTER_METHOD);
		map.put("phone", String.valueOf(phone));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 检查该账号是否注册
	 * 
	 * @param phone
	 * @param callBack
	 */
	public void checkPhone(long phone, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_USER_CHECK_PHONE_METHOD);
		map.put("userName", String.valueOf(phone));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}
	
	/**
	 *  上传头像
	 * @param bit
	 * @param fileType
	 * @param sessionId
	 * @param callBack
	 */
	public void uploadIcon(Bitmap bit, String fileType, String sessionId,
			GPBaseCallBack callBack) {

		Map<String, String> map = this.builderBaseMap();
		map.put("method", GPLUS_USER_UPLOAD_ICON_METHOD);
		map.put("sessionId", sessionId);

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bit.compress(CompressFormat.JPEG, 40, bos);// ����100��ʾ��ѹ��
		byte[] bytes = bos.toByteArray();
		String base64String = Base64.encodeToString(bytes, Base64.DEFAULT);

		map = this.builderSign(map);
		map.put("photo", fileType + "@" + base64String); // �ļ�������ǩ��
		GPHttpClient.asynPost(map, url, callBack);
	}
	
	/**
	 * 下载头像
	 * @param userName
	 * @param passoword
	 * @param callBack
	 */
//	public void downloadIcon(Bitmap bit, String fileType, String sessionId,
//			GPBaseCallBack callBack) {

//		Map<String, String> map = this.builderBaseMap();
//		map.put("method",GPLUS_USER_DOWNLOAD_ICON_METHOD);
//		map.put("sessionId", sessionId);
//
//		map = this.builderSign(map);
//		GPHttpClient.asynPost(map, url, callBack);
//	}


	/**
	 * 下载二维码
	 * 
	 * @param callBack
	 */
	public void downloadqrcode(String sessionId,String accountName, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_USER_DOWN_QRCODE_METHOD);
		map.put("sessionId", sessionId);
		map.put("accountName", accountName);
		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}
	/**
	 * 注册
	 * 
	 * @param phone
	 * @param passoword
	 * @param code
	 * @param callBack
	 */
	public void register(long phone, String passoword, int code,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method",GPLUS_USER_PHONE_REGISTER_METHOD);
		map.put("phone", String.valueOf(phone));
		map.put("password", GPSecretUtil.getSHA1DigestString(passoword));

		map.put("code", String.valueOf(code));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}
	
	
	/**
	 * 下载自己头像
	 * @param sessionId
	 * @param callBack
	 */
	public void download(String sessionId,String accountName,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_USER_DOWNLOAD_ICON_METHOD);
		map.put("sessionId", sessionId);
		map.put("accountName", accountName);


		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 获取个人信息
	 * 
	 * @param sessionId
	 * @param callBack
	 */
	public void getUserinfo(String sessionId, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_USER_GET_INFO_METHOD);
		map.put("sessionId", sessionId);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);
	}
	
	/**
	 * 上传随手记
	 * @param sessionId
	 * content,photo
	 * @param callBack
	 */
//	public void note(String sessionId,String content,ArrayList<String> photo,
//			GPBaseCallBack callBack) {
//		Map<String, String> map = this.builderBaseMap();
//
//		map.put("method", GPLUS_SHARE_LOG_NOTE_METHOD);
//		map.put("sessionId", sessionId);
//		map.put("content", content);
//		map.put("photo", String.valueOf(photo));
//
//
//		map = this.builderSign(map);
//
//		// 寮傛璋冪敤
//		GPHttpClient.asynPost(map, url, callBack);
//
//	}

	/**
	 * 更新个人信息
	 *
	 * @param sessionId
	 * @param nickname
	 * @param signature
	 * @param birthday
	 * @param gender
	 * @param district
	 * @param callBack
	 */
	public void updateUserinfo(String sessionId, String nickname,
			String signature, String birthday, int gender, int district,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method",GPLUS_USER_UPDATE_INFO_METHOD);
		map.put("sessionId", sessionId);

		map.put("nickname", nickname);
		map.put("signature", signature);
		map.put("birthday", birthday);
		map.put("gender", String.valueOf(gender));
		map.put("district", String.valueOf(district));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);
	}

	/**
	 * 发表随手记内容
	 * 
	 * @param callBack
	 */
	public void uploadLogWithNote(String sessionId, String content, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();
		map.put("method", GPLUS_LOG_SHARE_NOTE_METHOD);
		map.put("sessionId", sessionId);
		map.put("content", content);
		map = this.builderSign(map);
		GPHttpClient.asynPost(map, url, callBack);
	}
	
	/**
	 * 上传随手记图片
	 * 
	 * @param callBack
	 */
	public void uploadLogWithPhotos(String sessionId,String logId, String photoPathes, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();
		map.put("method", GPLUS_LOG_UPLOAD_PHOTO_METHOD); 
		map.put("sessionId", sessionId);
		map.put("logId", logId);
		map = this.builderSign(map);
//		for (String photoPath : photoPathes ) {
			Bitmap temp ;
			temp = BitmapFactory.decodeFile(photoPathes);
			
	        ByteArrayOutputStream bStream = new ByteArrayOutputStream();  
	        temp.compress(Bitmap.CompressFormat.JPEG, 20, bStream); 
	        byte[] bytes = bStream.toByteArray();  
	        	String base64Code = Base64.encodeToString(bytes, Base64.DEFAULT);       	
    	
	        	map.put("photo", "jpg@" + base64Code);
//	        }	
		//ClientHttp.asynPost(map, url, callBack);
	        	GPHttpClient.synPost(map, url, callBack);
	}
	
	/**
	 * 下载随手记图片
	 * 
	 * @param callBack
	 */
	public void downlaodLogPicture(String sessionId,String  logId ,
			String picttureId, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();
		map.put("method",GPLUS_LOG_DOWNLOAD_PHOTO_METHOD);
		map.put("sessionId", sessionId);
		map.put("LogId", logId);
		map.put("pictureId", picttureId);
		map = this.builderSign(map);
	        
		GPHttpClient.asynPost(map, url, callBack);
	}
	
	/*
	 *  下载随手记缩略图
	 */
	
	public void downlaodLogThumbnail(String sessionId,String  logId ,
			String picttureId, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();
		map.put("method", GPLUS_LOG_DOWNLOAD_THUMB_METHOD);
		map.put("sessionId", sessionId);
		map.put("LogId", logId);
		map.put("pictureId", picttureId);
		map = this.builderSign(map);
	        
		GPHttpClient.asynPost(map, url, callBack);
	}
	/**
	 * 删除日志
	 * 
	 * @param callBack
	 */
	public void deleteLog(String sessionId, String logId, GPClientCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();
		map.put("method", "share.delete.log");
		map.put("sessionId", sessionId);
		map.put("logId", logId);
		map = this.builderSign(map);
		
		GPHttpClient.asynPost(map, url, callBack);
		
	}
	
	/**
	 * getmyLoginfo
	 * 
	 * @param sessionId
	 * @param day
	 * @param daynum
	 * @param callBack
	 */
	public void getmyLog(String sessionId,String startDay, String endDay,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_LOG_SHARE_NOTE_METHOD);
		map.put("sessionId", sessionId);
		map.put("startDay",startDay);
		map.put("endDay",endDay );

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);
	}	
	
	/**
	 * 获取家庭成员列表
	 * @param sessionid
	 * @param callBack
	 */
	public void getfamily(String sessionid, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method",GPLUS_FIMILY_GET_MEMBER_METHOD);
		map.put("sessionId", sessionid);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}
	/**
	 * 增加家庭成员
	 * @param sessionid,phone
	 * @param callBack
	 */
	public void addfamily(String sessionid, Long  phone,String password,String  relation,GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_FIMILY_ADD_MEMBER_METHOD);
		map.put("sessionId", sessionid);
		map.put("phone", String.valueOf(phone));
		map.put("password",GPSecretUtil.getSHA1DigestString(password));
		map.put("relation", relation);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}
	
	/**
	 * 删除家庭成员
	 * @param sessionid,accountName
	 * @param callBack
	 */
	public void deletefamily(String sessionid,String  accountName,GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", GPLUS_FIMILY_DELETE_MEMBER_METHOD);
		map.put("sessionId", sessionid);
		map.put("accountName", accountName);
//		map.put("accountName", String.valueOf(accountName));

		map = this.builderSign(map);

		GPHttpClient.asynPost(map, url, callBack);

	}


}
