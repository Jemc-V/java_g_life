package com.gpluslife.service.http;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class GPHttpClient {

	
	private static HttpClient httpclient = new DefaultHttpClient();
	/**
	 * 同步访问网络，利用主线程
	 * 
	 * @param params
	 * @param url
	 */
	public static void synPost(Map<String, String> params, String url,
			GPBaseCallBack callBack) {
		try {
			 
			List<NameValuePair> parameters = new ArrayList<NameValuePair>(); 
			for (String key : params.keySet()) {
				 parameters.add(new BasicNameValuePair(key, params.get(key)));  
			}
			httpclient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 50000);
	        HttpEntity entity = new UrlEncodedFormEntity(parameters,"UTF-8");  
			String resultMsg = null;
			HttpEntity resultEntity = null;
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(entity);
			
			HttpResponse response = httpclient.execute(httpPost);
			int httpcode = response.getStatusLine().getStatusCode();
			System.out.println("httpcode == >" + httpcode);
			if (httpcode == HttpStatus.SC_OK) {
				resultEntity = response.getEntity();
				if (resultEntity != null) {
					resultMsg = EntityUtils.toString(resultEntity, "UTF-8");
					callBack.onSuccess(resultMsg);
				}

			} else {
				callBack.onHttpStatusError(httpcode);
			}

		} catch (Exception ex) {
			callBack.onError(ex);
		}

	}

	/**
	 * 异步访问网络，要用handler
	 * 
	 * @param params
	 * @param url
	 */
	public static void asynPost(final Map<String, String> params,
			final String url, final GPBaseCallBack callBack) {
		callBack.onStart();
		Runnable task = new Runnable() {
			@Override
			public void run() {
				synPost(params, url,callBack);
			}
		};
		new Thread(task).start();
	}
}
