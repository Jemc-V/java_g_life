package com.gpluslife.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import com.gpluslife.service.http.GPBaseCallBack;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.service.http.GPHttpClient;
import com.gpluslife.utils.GPSecretUtil;


public class GPUserService extends GPBaseService {

	/**
	 * 登录
	 * 
	 * @param userName
	 * @param passoword
	 * @param callBack
	 */
	public void login(String userName, String passoword, String deviceKey,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.login");
		map.put("userName", userName);
		map.put("password", GPSecretUtil.getSHA1DigestString(passoword));
		map.put("deviceKey", deviceKey);
		// deviceKey 手机唯一码

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	public void checklogin(String userName, String passoword, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.login");
		map.put("userName", userName);
		map.put("password", passoword);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	public void reset(int code, Long phone, String passoword, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.reset.password");
		map.put("code", String.valueOf(code));
		map.put("phone", String.valueOf(phone));
		map.put("passoword", GPSecretUtil.getSHA1DigestString(passoword));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 鑾峰彇鐭俊
	 * 
	 * @param phone
	 * @param callBack
	 */
	public void getSMS(long phone, GPClientCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "sms.register");
		map.put("phone", String.valueOf(phone));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 鏍￠獙鎵嬫満鍙风爜
	 * 
	 * @param phone
	 * @param callBack
	 */
	public void checkPhone(long phone, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.check");
		map.put("userName", String.valueOf(phone));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 下载二维码
	 * 
	 * @param callBack
	 */
	public void downloadqrcode(String sessionId, String accountName,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.download.qrcode");
		map.put("sessionId", sessionId);
		map.put("accountName", accountName);
		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 注册
	 * 
	 * @param phone
	 * @param passoword
	 * @param code
	 * @param callBack
	 */
	public void register(long phone, String passoword, int code,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.phone.register");
		map.put("phone", String.valueOf(phone));
		map.put("password", GPSecretUtil.getSHA1DigestString(passoword));

		map.put("code", String.valueOf(code));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 下载自己头像
	 * 
	 * @param sessionId
	 * @param callBack
	 */
	public void download(String sessionId, String accountName, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.download.icon");
		map.put("sessionId", sessionId);
		map.put("accountName", accountName);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 获取个人信息
	 * 
	 * @param sessionId
	 * @param callBack
	 */
	public void getUserinfo(String sessionId, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.get.info");
		map.put("sessionId", sessionId);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);
	}

	/**
	 * 随手记
	 * 
	 * @param sessionId
	 *            content,photo
	 * @param callBack
	 */
	public void note(String sessionId, String content, ArrayList<String> photo,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "share.log.note");
		map.put("sessionId", sessionId);
		map.put("content", content);
		map.put("photo", String.valueOf(photo));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 更新个人形象
	 * 
	 * @param sessionId
	 * @param nickname
	 * @param signature
	 * @param birthday
	 * @param gender
	 * @param district
	 * @param callBack
	 */
	public void updateUserinfo(String sessionId, String nickname,
			String signature, String birthday, int gender, int district,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "user.update.info");
		map.put("sessionId", sessionId);

		map.put("nickname", nickname);
		map.put("signature", signature);
		map.put("birthday", birthday);
		map.put("gender", String.valueOf(gender));
		map.put("district", String.valueOf(district));

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);
	}

	/**
	 * getmyLoginfo
	 * 
	 * @param sessionId
	 * @param day
	 * @param daynum
	 * @param callBack
	 */
	public void getmyLog(String sessionId, String pageNum, String pageSize,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "share.page.logs");
		map.put("sessionId", sessionId);
		map.put("pageNum", pageNum);
		map.put("pageSize", pageSize);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);
	}

	/**
	 * 鍗曚緥
	 */
	private GPUserService() {

	}

	public static GPUserService get() {
		return Holder.instance;
	}

	// 鍦ㄧ锟�锟斤拷琚紩鐢ㄦ椂琚姞锟�
	private static class Holder {
		private static GPUserService instance = new GPUserService();
	}

	/**
	 * 获取家庭成员列表
	 * 
	 * @param sessionid
	 * @param callBack
	 */
	public void getfamily(String sessionid, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "family.get.member");
		map.put("sessionId", sessionid);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 增加家庭成员
	 * 
	 * @param sessionid
	 *            ,phone
	 * @param callBack
	 */
	public void addfamily(String sessionid, Long phone, String password,
			String relation, GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "family.add.member");
		map.put("sessionId", sessionid);
		map.put("phone", String.valueOf(phone));
		map.put("password", GPSecretUtil.getSHA1DigestString(password));
		map.put("relation", relation);

		map = this.builderSign(map);

		// 寮傛璋冪敤
		GPHttpClient.asynPost(map, url, callBack);

	}

	/**
	 * 删除家庭成员
	 * 
	 * @param sessionid
	 *            ,accountName
	 * @param callBack
	 */
	public void deletefamily(String sessionid, String accountName,
			GPBaseCallBack callBack) {
		Map<String, String> map = this.builderBaseMap();

		map.put("method", "family.delete.member");
		map.put("sessionId", sessionid);
		map.put("accountName", accountName);
		// map.put("accountName", String.valueOf(accountName));

		map = this.builderSign(map);

		GPHttpClient.asynPost(map, url, callBack);

	}

}
