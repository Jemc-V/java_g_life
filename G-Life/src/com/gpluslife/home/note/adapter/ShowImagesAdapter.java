package com.gpluslife.home.note.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.gpluslife.R;
import com.gpluslife.utils.NativeImageLoader;


public class ShowImagesAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private Context mContext;
    private GridView mGridView;
    private List<String> mList;//保存选中目录下的图片地址
    private List<String> mAllSelectedImages;
    private Handler mHandler;
    private int mImgHeight = 128;//测量图片的高度
    private int mImgWidth = 130;//测量图片的宽度
    private String mDirPath;
    /**
     * 用来存储图片的选中情况
     */
    private boolean mIsSelected;
    
    private ArrayList<String> mTempSelected = new ArrayList<String>();
    private int mFristVisibleItem;
    private int mVisibleItemCount;
    private boolean isFirstEnterThisActivity = true;
    private ArrayList<Integer> mIsChecked = new ArrayList<Integer>();//存储选中图片的位置
    String actionBarTitle = "";
    /**
     * 
     * @param context 
     * @param list 选中目录下图片名的集合
     * @param paths 已经选中的图片路径集合（包含拍照）
     * @param dirPath 选中目录的路径
     * @param gridView 
     * @param handler
     */
    public ShowImagesAdapter(Context context, List<String> list, List<String> paths, String dirPath, GridView gridView, Handler handler) {
        this.mContext = context;
        mList = list;
        mAllSelectedImages = paths;
        mDirPath = dirPath;
        mGridView = gridView;
        mHandler = handler;
        mInflater = LayoutInflater.from(context);

        mGridView.setOnScrollListener(new ShowImagesOnScrollListener());
    }

    public ArrayList<String> getSelectMap() {
        return mTempSelected;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        final String path = mDirPath + File.separator + mList.get(position);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_image_grid, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.mSelected = (ImageView) convertView.findViewById(R.id.isselected);
            viewHolder.mTextView = (TextView) convertView.findViewById(R.id.item_image_grid_text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.mImageView.setImageResource(R.drawable.picture_no);
        }
        viewHolder.mSelected.setVisibility(View.INVISIBLE);
        viewHolder.mTextView.setVisibility(View.INVISIBLE);
        viewHolder.mImageView.setTag(path);

        viewHolder.mImageView.setOnClickListener(new View.OnClickListener() {
			@Override
            public void onClick(View view) {
				
            	if(viewHolder.mSelected.getVisibility() == View.INVISIBLE){
            		mIsSelected = true;
            		mIsChecked.add(position);
            	}else{
            		mIsSelected = false;
            		mIsChecked.remove(new Integer(position));
            	}
            	if(null == mAllSelectedImages){
            		setCheckedJustSelectedImages(path, mIsSelected, viewHolder);
            	}else{
            		setCheckedTakePhoto(path, mIsSelected, viewHolder);
            	}
            }
        });

        
        

        

        if(mIsChecked.contains(position)){
        	viewHolder.mSelected.setVisibility(View.VISIBLE);
            viewHolder.mTextView.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    private void sendTotalSelected(){
    	if (null != mAllSelectedImages) {
            actionBarTitle = mAllSelectedImages.size() + mTempSelected.size() + "/" + 9;
        } else {
            actionBarTitle = mTempSelected.size() + "/" + 9;
        }
    	Message msg = new Message();
        msg.obj = actionBarTitle;
        mHandler.sendMessage(msg);
    }
    public class ViewHolder {
        public ImageView mImageView;
        public ImageView mSelected;
        public TextView mTextView;
    }


    /**
     * 用户有拍照行为和选择照片行为
     *
     * @param path
     * @param isChecked
     * @param viewHolder
     */
    private void setCheckedTakePhoto(String path, boolean isChecked, ViewHolder viewHolder) {
        if (mAllSelectedImages.size() + mTempSelected.size() < 9) {
            setCheckedInPermission(path, isChecked, viewHolder);
        } else if (mAllSelectedImages.size() + mTempSelected.size() == 9 && !isChecked) {
            setCheckedInPermission(path, isChecked, viewHolder);
        } else {
            setCheckedNotInPermission(path, isChecked, viewHolder);
        }
    }

    /**
     * 用户只有选择照片行为
     *
     * @param path
     * @param isChecked
     * @param viewHolder
     */
    private void setCheckedJustSelectedImages(String path, boolean isChecked, ViewHolder viewHolder) {
        if (mTempSelected.size() < 9) {
            setCheckedInPermission(path, isChecked, viewHolder);
        } else if (mTempSelected.size() == 9 && !isChecked) {
            setCheckedInPermission(path, isChecked, viewHolder);
        } else {
            setCheckedNotInPermission(path, isChecked, viewHolder);
        }
    }

    /**
     * 用户选择的照片在可选择张数范围内
     *
     * @param path
     * @param isChecked
     * @param viewHolder
     */
    private void setCheckedInPermission(String path, boolean isChecked, ViewHolder viewHolder) {
        if (isChecked) {
            mTempSelected.add(path);
            viewHolder.mSelected.setVisibility(View.VISIBLE);
    		viewHolder.mTextView.setVisibility(View.VISIBLE);
        } else {
        	mTempSelected.remove(path);
            viewHolder.mSelected.setVisibility(View.INVISIBLE);
    		viewHolder.mTextView.setVisibility(View.INVISIBLE);
        }
        sendTotalSelected();
    }

    /**
     * 用户选择的照片已经超过可选择张数
     *
     * @param path
     * @param isChecked
     * @param viewHolder
     */
    private void setCheckedNotInPermission(String path, boolean isChecked, ViewHolder viewHolder) {
        if (isChecked) {
            //传递数据到ShowImageActivity，通知用户已经选择了9张图片，不可以再选择
            mHandler.sendEmptyMessage(-1);
            //viewHolder.mSelected.setChecked(!isChecked);
            viewHolder.mSelected.setVisibility(View.INVISIBLE);
            viewHolder.mTextView.setVisibility(View.INVISIBLE);
        } else {
            mTempSelected.remove(path);
        }
    }

    private class ShowImagesOnScrollListener implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView absListView, int scrollState) {
            absListView.getFirstVisiblePosition();
            if (scrollState == SCROLL_STATE_IDLE) {
                loadBitmaps(mFristVisibleItem, mVisibleItemCount);
            }
        }

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {
            mFristVisibleItem = firstVisibleItem;
            mVisibleItemCount = visibleItemCount;
            if (isFirstEnterThisActivity && visibleItemCount > 0) {
                
            	
            	loadBitmaps(firstVisibleItem, visibleItemCount);
                isFirstEnterThisActivity = false;
            }
        }
    }

    private void loadBitmaps(int firstVisibleItem, int visibleItemCount) {
        try {
            for (int i = firstVisibleItem; i < firstVisibleItem
                    + visibleItemCount; i++) {
                String imgPath = mDirPath + File.separator + mList.get(i);
                ImageView imageView = (ImageView) mGridView
                        .findViewWithTag(imgPath);
                Bitmap bitmap = NativeImageLoader.getInstance().loadNativeImage(imgPath, mImgWidth, mImgHeight, new NativeImageLoader.NativeImageCallBack() {

                    @Override
                    public void onImageLoader(Bitmap bitmap, String path) {
                        ImageView imageView = (ImageView) mGridView.findViewWithTag(path);
                        if (null != bitmap && null != imageView) {
                            imageView.setImageBitmap(bitmap);
                        }
                    }
                });

                if (null != bitmap) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
