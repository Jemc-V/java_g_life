package com.gpluslife.home.note;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.gpluslife.R;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.filemanager.GPFileManager;
import com.gpluslife.home.GPHomeActivity;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.login.model.GPUserModelManager;
import com.gpluslife.utils.GPConstants;
import com.gpluslife.utils.NativeImageLoader;

public class NoteActivity extends Activity {

	protected static final String TAG = "NoteActivity";
	private GridView noScrollgridview;
	private GridAdapter adapter;
	private Button send;
	private EditText txt;
	private String sessionId, content;
	private GPNavigationView navigationView;
	private Button back;
	public static List<Bitmap> bmp = new ArrayList<Bitmap>();
	private ShareNoteContent shareNoteContent;
	NoteUploadingPhotoLoadingDialog dialog;

	//拍照
	private static final int REQUEST_CAPTURE_IMAGE = 1;
	private static final int REQUEST_SELECT_IMAGE = 2;
	private static final int REQUEST_CHECK_IMAGE = 3;
	// 拍照存储
	private File mImageCaptured;
	//存储用户选中的图片路径，用List可以选择重复的图片
	private ArrayList<String> mSelectedImages = new ArrayList<String>();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.note_selectimg);
		// ===============================获取sessionId、logId=======================
		sessionId = GPLoginUserModel.get().getSessionId();

		// ===========================================================================
		shareNoteContent = new ShareNoteContent();
		dialog = new NoteUploadingPhotoLoadingDialog(this);

		// ============================================================================
		navigationView = (GPNavigationView) findViewById(R.id.logNavigationView);
		txt = (EditText) findViewById(R.id.txt);
		back = navigationView.getBtn_left();
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*Intent intent = new Intent(NoteActivity.this,
						GPHomeActivity.class);
				startActivity(intent);*/
				NoteActivity.this.finish();
			}
		});
		Init();
	}

	// =================================发送随手记事件=============================
	public void Init() {
		noScrollgridview = (GridView) findViewById(R.id.noScrollgridview);
		noScrollgridview.setSelector(new ColorDrawable(Color.TRANSPARENT));
		adapter = new GridAdapter();
		/*adapter.update();*/
		noScrollgridview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		noScrollgridview.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if (arg2 == mSelectedImages.size()) {
					new PopupWindows(NoteActivity.this, noScrollgridview);
				} else {
					Intent intent = new Intent(NoteActivity.this,
							PhotoActivity.class);
					intent.putStringArrayListExtra(GPConstants.LIST_SELECTED_PATH, (ArrayList<String>) mSelectedImages);
					intent.putExtra(GPConstants.CURRENT_CLICK_POSITION, arg2);
					startActivityForResult(intent, REQUEST_CHECK_IMAGE);
				}
			}
		});
		send = navigationView.getBtn_right();
		send.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				content = txt.getEditableText().toString();
				if (txt.getEditableText().toString().length() == 0
						&& txt.getEditableText().toString().length() <= 500) {
					Toast.makeText(NoteActivity.this, R.string.notetxt,
							Toast.LENGTH_LONG).show();
				} else {
					Log.d(TAG, "sessionId=" + sessionId + "content=" + content);

					// 随手记上传

					dialog.setCanceledOnTouchOutside(false);
					dialog.show();
					shareNoteContent.notecontent(NoteActivity.this, sessionId,
							content, dialog, mSelectedImages);
				}

			}
		});

	}

	@SuppressLint("HandlerLeak")
	public class GridAdapter extends BaseAdapter {
		private LayoutInflater inflater; // 视图容器
		
		public GridAdapter(){
			inflater = LayoutInflater.from(NoteActivity.this);
		}
		
		@Override
		public View getView(int position, View view, ViewGroup parent) {String path = "";
		if (position < mSelectedImages.size()) {
			path = mSelectedImages.get(position);
		}
		ViewHolder holder = null;
		if (view == null) {
			view = inflater.inflate(R.layout.item_published_grida,parent, false);
			holder = new ViewHolder();
			holder.image = (ImageView) view.findViewById(R.id.item_grida_image);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.image.setTag(path);
		System.out.println("current position:" + position);
		if (position == mSelectedImages.size()) {
			// 选择的图片小于9张时，在后面有个添加图片
			holder.image.setImageResource(R.drawable.note_add_img);
			if (position == 9) {
				// 选择为9张时，添加图片 消失
				holder.image.setVisibility(View.GONE);
			}
		} else {
			Bitmap bitmap = NativeImageLoader.getInstance().loadNativeImage(
					path, holder.image.getWidth(), holder.image.getHeight(),
					new NativeImageLoader.NativeImageCallBack() {

						@Override
						public void onImageLoader(Bitmap bitmap, String path) {
							ImageView mImageView = (ImageView) noScrollgridview
									.findViewWithTag(path);
							if (bitmap != null && mImageView != null) {
								mImageView.setImageBitmap(bitmap);
							}
						}
					});

			if (bitmap != null) {
				holder.image.setImageBitmap(bitmap);
			} else {
				holder.image.setImageResource(R.drawable.picture_no);
			}
		}
		return view;
	}

		public class ViewHolder {
			public ImageView image;
		}

		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
					adapter.notifyDataSetChanged();
					break;
				}
				super.handleMessage(msg);
			}
		};


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mSelectedImages.size()+1;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

	}


	/*protected void onRestart() {
		adapter.update();
		super.onRestart();
	}*/

	public class PopupWindows extends PopupWindow {

		public PopupWindows(Context mContext, View parent) {

			View view = View
					.inflate(mContext, R.layout.item_popupwindows, null);
			view.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.fade_ins));
			LinearLayout ll_popup = (LinearLayout) view
					.findViewById(R.id.ll_popup);
			ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.push_bottom_in_2));

			setWidth(LayoutParams.MATCH_PARENT);
			setHeight(LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button bt1 = (Button) view
					.findViewById(R.id.item_popupwindows_camera);
			Button bt2 = (Button) view
					.findViewById(R.id.item_popupwindows_Photo);
			Button bt3 = (Button) view
					.findViewById(R.id.item_popupwindows_cancel);
			bt1.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					photo();
					dismiss();
				}
			});
			bt2.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Intent intent = new Intent(NoteActivity.this,
							NotePicActivity.class);
					if(mSelectedImages.size()>0){
						intent.putStringArrayListExtra(GPConstants.LIST_ALL_SELECTED_IMAGES, (ArrayList<String>) mSelectedImages);
					}
					startActivityForResult(intent,REQUEST_SELECT_IMAGE);
					dismiss();
				}
			});
			bt3.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}


	public void photo() {
		PackageManager manager = getPackageManager();
		if (null == manager) {
			Log.e(TAG, "takeImage - getPackageManager FAILED.");
			return;
		}

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (null == intent.resolveActivity(manager)) {
			Log.e(TAG, "takeImage - resolveActivity FAILED.");
			return;
		}

		mImageCaptured = createImage();
		if (null == mImageCaptured) {
			Log.e(TAG, "takeImage - create image file FAILED.");
			return;
		}
		
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mImageCaptured));
		startActivityForResult(intent, REQUEST_CAPTURE_IMAGE);
	}

	@SuppressLint("NewApi")
	private File createImage() {
        if (!GPFileManager.getFileManager().isWriteAndRead()) {
        	Log.e(TAG, "SDCard is not writeadle");
            return null;
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        File file;
        try {
            file = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (Exception e) {
            Log.e(TAG, "createImage - createTempFile FAILED: " + e.toString());
            return null;
        }

        return file;
    }
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode != RESULT_OK){
			return;
		}
		switch (requestCode) {
			case REQUEST_CAPTURE_IMAGE: {
	            handleResultOfCaptureImage(data);
	            break;
	        }
	        case REQUEST_SELECT_IMAGE: {
	            handleResultOfSelectImage(data);
	            break;
	        }
	        case REQUEST_CHECK_IMAGE:{
	        	handleResultOfNewSelectImage(data);
	        	break;
	        }
		}
	}

	private void handleResultOfCaptureImage(Intent data) {
		// TODO Auto-generated method stub
		mSelectedImages.add(mImageCaptured.getAbsolutePath());
		adapter.notifyDataSetChanged();
		//Log.e(TAG, "handleResultOfCaptureImage " + mImageCaptured.getAbsolutePath());
		//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
		//的目录为/storage/sdcard1/Pictures/JPEG_20140917_170032851_-1512164137.jpg

	}

	private void handleResultOfSelectImage(Intent data) {
		// TODO Auto-generated method stub
		List<String> selectedPaths = data.getStringArrayListExtra(GPConstants.LIST_SELECTED_PATH);
		mSelectedImages.addAll(selectedPaths);
		adapter.notifyDataSetChanged();
	}
	private void handleResultOfNewSelectImage(Intent data) {
		// TODO Auto-generated method stub
		if(data != null){
			List<Integer> deletePositions = data.getIntegerArrayListExtra(GPConstants.LIST_DELETE_POSITION);
			for(int position : deletePositions){
				mSelectedImages.remove(position);
			}
			adapter.notifyDataSetChanged();
		}
	}
}
