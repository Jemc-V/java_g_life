package com.gpluslife.home.note;

import com.gpluslife.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NoteUploadingPhotoLoadingDialog extends Dialog {
	private TextView tv;

	public NoteUploadingPhotoLoadingDialog(Context context) {
		super(context, R.style.loadingDialogStyle);
	}

	private NoteUploadingPhotoLoadingDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.note_update_photo_dialog_loading);
		tv = (TextView)this.findViewById(R.id.tv);
		tv.setText("正在上传...");
		LinearLayout linearLayout = (LinearLayout)this.findViewById(R.id.LinearLayout);
		linearLayout.getBackground().setAlpha(210);
	}
}
