package com.gpluslife.home.fragment.adapter;

import java.util.ArrayList;

import com.gpluslife.R;
import com.gpluslife.utils.GPBitmapUtils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GPHomeAdapter extends BaseAdapter {

	private static String TAG = "GPHomeAdapter";
	
	private Activity mContext;
	private LayoutInflater mInflater;
	private ArrayList<ArrayList<String>> mPluginList;
	
	public GPHomeAdapter()
	{
		
	}
	
	public GPHomeAdapter(Activity context, ArrayList<ArrayList<String>> list)
	{
		mContext = context;
		mPluginList = list;
		mInflater = (LayoutInflater) mContext
		.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		int count = mPluginList.size();
		return count;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		
		ArrayList<String> list = mPluginList.get(position);
		
		return list;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		convertView = mInflater.inflate(R.layout.scale_main_item	,null);
		if(convertView != null)
		{
			ArrayList<String> list = (ArrayList<String>)getItem(position);
			
			ImageView iconImageView = (ImageView)convertView.findViewById(R.id.icon);
			TextView nameTV = (TextView)convertView.findViewById(R.id.name);
			
			Log.d(TAG,  "list  == "+  list.get(1).toString());
			Bitmap bitmap = GPBitmapUtils.getImageFromAssetsFile(mContext, list.get(1).toString());
			iconImageView.setImageBitmap(bitmap);
			
			nameTV.setText(list.get(0));
		}
		return convertView;
	}

}
