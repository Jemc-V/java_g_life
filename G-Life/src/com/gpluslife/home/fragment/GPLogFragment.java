package com.gpluslife.home.fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

import com.gpluslife.R;
import com.gpluslife.database.GPDatabaseActivity;
import com.gpluslife.home.log.unit.GetServiceToDb;
import com.gpluslife.home.log.unit.Getdbfile;
import com.gpluslife.home.log.unit.LogAdapterNotes;
import com.gpluslife.home.log.unit.ModleTextandView;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.pullrefresh.GPPullToRefreshView;
import com.gpluslife.pullrefresh.GPPullToRefreshView.OnFooterRefreshListener;
import com.gpluslife.pullrefresh.GPPullToRefreshView.OnHeaderRefreshListener;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPDelectOrShare;

public class GPLogFragment extends Fragment implements OnHeaderRefreshListener,
		OnFooterRefreshListener {



	private String TAG = "Log_Fragment";
	private String sessionId = null;
	private String phone = null;
	private ListView log_list;
	private Getdbfile getdbfile;
	public GetServiceToDb getServicedb;
	public SQLiteDatabase sqlitedb;
	private int page = 0;
	private int OldListY = -1;
	public LogAdapterNotes logAdapter;
	private List<ModleTextandView> dataArrayList = new ArrayList<ModleTextandView>();
	private GPPullToRefreshView mPullToRefreshView;
	private GPLoginUserModel userModel;
	private List<ModleTextandView> DelectdataAarrays = new ArrayList<ModleTextandView>();
	
	
	
	
	
	public GPLogFragment() {
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_log, container,
				false);
		SharedPreferences sp = getActivity().getSharedPreferences("usermodel",
				getActivity().MODE_WORLD_READABLE);
		sessionId = sp.getString("sessionId", sessionId);
		log_list = (ListView) rootView.findViewById(R.id.log_list);
		
		log_list.setOnItemLongClickListener(new ListViewHandle());
//		getServicedb = new GetServiceToDb(getActivity());
//		getServicedb.WriteToDb(sessionId, String.valueOf(1));
//		getdbfile = new Getdbfile(getActivity());

		GPDatabaseActivity dbhelp = new GPDatabaseActivity(getActivity());
		sqlitedb = dbhelp.getWritableDatabase();
		SendDataToAdapter(page);

		mPullToRefreshView = (GPPullToRefreshView) rootView
				.findViewById(R.id.main_pull_refresh_view);
		mPullToRefreshView.setOnHeaderRefreshListener(this);
		mPullToRefreshView.setOnFooterRefreshListener(this);

		return rootView;
	}

	/*
	 * 启动异步线程传值到Adapter，加载数据
	 */

	public void SendDataToAdapter(final int page) {
		final Handler handler = new Handler() {
			@SuppressWarnings("unchecked")
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				logAdapter = new LogAdapterNotes(getActivity(),
						(List<ModleTextandView>) msg.obj, sessionId);
				log_list.setAdapter(logAdapter);

			}
		};
		new Thread() {
			public void run() {

				Getdbfile getdbfile = new Getdbfile(getActivity());
				List<ModleTextandView> dataAarrays = new ArrayList<ModleTextandView>();
				dataAarrays = getdbfile.getModleId(page);
				Message msg = new Message();
				msg.obj = dataAarrays;
				handler.sendMessage(msg);

			}
		}.start();
	}

	@Override
	public void onFooterRefresh(GPPullToRefreshView view) {
		// TODO 自动生成的方法存根
		mPullToRefreshView.postDelayed(new Runnable() {

			@Override
			public void run() {

				int pages = ++page;

				getServicedb = new GetServiceToDb(getActivity());
				getServicedb.WriteToDb(sessionId, String.valueOf(pages));
				getdbfile = new Getdbfile(getActivity());
				Cursor c = sqlitedb.query(GPDatabaseActivity.TABLE_NAME, null,
						null, null, null, null, null);
				int count = c.getCount();

				if (count > pages * 20) {
					Getdbfile getdbfile = new Getdbfile(getActivity());
					List<ModleTextandView> dataAarrays = new ArrayList<ModleTextandView>();
					dataAarrays = getdbfile.getModleId(pages);
					System.out.println("Fragment home下拉加载更多获取数据"
							+ dataAarrays.size());
					logAdapter.addNewsItem(dataAarrays);
					logAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(getActivity(), "数据加载完毕", 500).show();
				}
				mPullToRefreshView.onFooterRefreshComplete();
			}
		}, 1000);

	}

	@Override
	public void onHeaderRefresh(GPPullToRefreshView view) {
		// TODO 自动生成的方法存根
		mPullToRefreshView.postDelayed(new Runnable() {

			@Override
			public void run() {
				// 设置更新时间
				getServicedb = new GetServiceToDb(getActivity());
				getServicedb.WriteToDb(sessionId, String.valueOf(1));
				SendDataToAdapter(0);

				mPullToRefreshView.onHeaderRefreshComplete("最近更新:01-23 12:01");
				mPullToRefreshView.onHeaderRefreshComplete();
			}
		}, 1000);
	}
	
	
	
	private class ListViewHandle implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(final AdapterView<?> groupView,
				final View view, int arg2, long arg3) {
			final ModleTextandView ImageAndText;
			ImageAndText = logAdapter.getItem(arg2);
			
			DelectdataAarrays = logAdapter.getlistModleTextandViews();
			
			DelectdataAarrays.remove(ImageAndText);
			int[] location = new int [2];
			view.getLocationOnScreen(location);
			OldListY = location[1];
			final GPDelectOrShare selectDialog = new GPDelectOrShare(getActivity(),
					R.style.dialog);// 创建Dialog并设置样式主题
			Window win = selectDialog.getWindow();
			LayoutParams params = new LayoutParams();
			params.y = OldListY-500;// 设置y坐标
			win.setAttributes(params);
			selectDialog.show();
			selectDialog.setCanceledOnTouchOutside(true);// 设置点击Dialog外部任意区域关闭Dialog	
			TextView delete = (TextView) selectDialog.findViewById(R.id.yichu);
			TextView share = (TextView) selectDialog.findViewById(R.id.gongxiang);
			delete.setOnClickListener(new OnClickListener() {
				@SuppressLint("ShowToast")
				@Override
				public void onClick(View arg0) {
					
					logAdapter = new LogAdapterNotes(getActivity(),
							DelectdataAarrays, sessionId);
					log_list.setAdapter(logAdapter);
					log_list.invalidate();

					
					GPRequestService.get().deleteLog(sessionId,
							ImageAndText.getLOGID(), new GPClientCallBack() {
								@SuppressLint("NewApi")
								public void onSuccess(String jsonString)
										throws Exception {
									System.out.println("查看服务器的删除的JSON数据："+ jsonString);
									JSONObject jsonObj = new JSONObject(jsonString);
									int code = jsonObj.getInt("code");
									
									if (code == 200) {
										String where = ImageAndText.getLOGID();
										System.out.println("查看服务where：" + where);
										/*
										 * 判断获取到的数据是否已经存在数据库
										 */
										String selection = "logid =? ";
										String[] selections = new String[] { where };
										int id = sqlitedb.delete(GPDatabaseActivity.TABLE_NAME,selection, selections);
										System.out.println("查看服务器的删除的数据：" + id);
										Cursor c =sqlitedb.query(GPDatabaseActivity.TABLE_NAME,null, null, null, null, null, null);
										System.out.println("移除后数据库还剩的数据数目"+ c.getCount());
								
									} else if (code == 102001) {
										Log.d(TAG, "No data !");
										Looper.prepare();
										Toast.makeText(getActivity(), "没有数据!",500).show();
										Looper.loop();
									} else {
										Looper.prepare();
										Toast.makeText(getActivity(), "获取数据失败",500).show();
										Looper.loop();
									}
								}
							});
					
					
					
					
					selectDialog.cancel();
				}
			});
			share.setOnClickListener(new OnClickListener() {
				@SuppressLint("ShowToast")
				@Override
				public void onClick(View arg0) {
//					showShare(view);
					selectDialog.cancel();
				}
			});
			return true;
		}
	}
	

}
