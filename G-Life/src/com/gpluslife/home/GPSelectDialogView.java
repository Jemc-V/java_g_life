package com.gpluslife.home;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TableRow;

import com.gpluslife.R;

public class GPSelectDialogView extends AlertDialog {

	private TableRow logTableRow;
	private TableRow qRCodeTableRow;
	private TableRow functionTableRow;
	private TableRow accountTableRow;
	
	public GPSelectDialogView(Context context, int theme) {
		super(context, theme);
	}

	public GPSelectDialogView(Context context) {
		super(context,THEME_HOLO_DARK);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_dialog_view);
		logTableRow = (TableRow)findViewById(R.id.frame_01);
		qRCodeTableRow = (TableRow)findViewById(R.id.frame_02);
		functionTableRow = (TableRow)findViewById(R.id.frame_03);
		accountTableRow = (TableRow)findViewById(R.id.frame_04);
	}

	public TableRow getLogTableRow() {
		return logTableRow;
	}

	public void setLogTableRow(TableRow logTableRow) {
		this.logTableRow = logTableRow;
	}

	public TableRow getqRCodeTableRow() {
		return qRCodeTableRow;
	}

	public void setqRCodeTableRow(TableRow qRCodeTableRow) {
		this.qRCodeTableRow = qRCodeTableRow;
	}

	public TableRow getFunctionTableRow() {
		return functionTableRow;
	}

	public void setFunctionTableRow(TableRow functionTableRow) {
		this.functionTableRow = functionTableRow;
	}

	public TableRow getAccountTableRow() {
		return accountTableRow;
	}

	public void setAccountTableRow(TableRow accountTableRow) {
		this.accountTableRow = accountTableRow;
	}
	
	
	

}