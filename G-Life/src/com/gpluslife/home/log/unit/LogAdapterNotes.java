package com.gpluslife.home.log.unit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.gpluslife.R;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LogAdapterNotes extends BaseAdapter {
	public List<ModleTextandView> ImageAndText;
	private FragmentActivity activity;
	public BitmapLoaders asyncBitmapLoader;
	LayoutInflater inflater;
	private String groupkey;
	public String sesionid;

	public LogAdapterNotes(FragmentActivity fragmentActivity,
			List<ModleTextandView> imageAndText, String sessionID) {
		activity = fragmentActivity;
		this.ImageAndText = imageAndText;
		this.sesionid = sessionID;
		asyncBitmapLoader = new BitmapLoaders(activity.getApplicationContext());
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ImageAndText.size();
	}

	@Override
	public ModleTextandView getItem(int arg0) {
		// TODO Auto-generated method stub
		return ImageAndText.get(arg0);
	}

	public List<ModleTextandView> getlistModleTextandViews() {
		return ImageAndText;
		
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@SuppressWarnings("null")
	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		final ModleTextandView imageAndText;
		imageAndText = getItem(arg0);
		Log.d("LogAdapterNotes", "数据长度" + getCount());
		View v = arg1;

		groupkey = imageAndText.getALLTIME();

		Log.d("LogAdapterNotes", "查看groupkey" + imageAndText.getALLTIME());
		if (groupkey == null) {

			if (imageAndText.getPICTURES().size() == 0) {
				v = inflater.inflate(R.layout.log_list_user_item, null);

				TextView tv = (TextView) v.findViewById(R.id.set_txt);
				tv.setText(imageAndText.getCONTENT());

			} else if (imageAndText.getPICTURES().size() == 1) {
				v = inflater.inflate(R.layout.log_list_user_item_img3, null);
				final int which = 0;
				int leng = imageAndText.getPICTURES().size();
				final String[] arr = (String[]) imageAndText.getPICTURES()
						.toArray(new String[leng]);
				TextView tv = (TextView) v.findViewById(R.id.set_txt);
				ImageView imgs = (ImageView) v.findViewById(R.id.set_imge01);
				tv.setText(imageAndText.getCONTENT());
				asyncBitmapLoader.DisplayImage(imgs, sesionid, imageAndText
						.getLOGID(), imageAndText.getPICTURES().get(0));

				imgs.setVisibility(View.VISIBLE);
				imgs.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO 自动生成的方法存根
//						Intent i = new Intent(activity, LogPicture.class);
//						i.putExtra("url", arr);
//						i.putExtra("logid", imageAndText.getLOGID());
//						i.putExtra("sessionid", sesionid);
//						i.putExtra("whichurl", which);
//						i.putExtra("imagesCount", 1);
//						i.putExtra("cacheThumbnail",
//								asyncBitmapLoader.fileCache);
//						activity.startActivity(i);

					}

				});
			} else {
				v = inflater.inflate(R.layout.log_list_user_item_img, null);
				int Rid[] = { R.id.set_imge01, R.id.set_imge02,
						R.id.set_imge03, R.id.set_imge04, R.id.set_imge05,
						R.id.set_imge06, R.id.set_imge07, R.id.set_imge08,
						R.id.set_imge09 };
				int leng = imageAndText.getPICTURES().size();
				final String[] arr = (String[]) imageAndText.getPICTURES()
						.toArray(new String[leng]);
				TextView tv = (TextView) v.findViewById(R.id.set_txt);
				tv.setText(imageAndText.getCONTENT());
				final ArrayList<ImageView> listImageView = new ArrayList<ImageView>();
				for (int i = 0; i < leng; i++) {
					// picurl[i] =imageAndText.getPICTURES().get(i);
					ImageView imgs = (ImageView) v.findViewById(Rid[i]);
					listImageView.add(imgs);
				}

				for (int j = 0; j < listImageView.size(); j++) {
					final int which = j;
					asyncBitmapLoader.DisplayImage(listImageView.get(j),
							sesionid, imageAndText.getLOGID(), imageAndText
									.getPICTURES().get(j));
					listImageView.get(j).setVisibility(View.VISIBLE);
					listImageView.get(j).setOnClickListener(
							new OnClickListener() {

								@Override
								public void onClick(View arg0) {
									// TODO 自动生成的方法存根
//									Intent i = new Intent(activity,
//											LogPicture.class);
//									i.putExtra("url", arr);
//									i.putExtra("logid", imageAndText.getLOGID());
//									i.putExtra("sessionid", sesionid);
//									i.putExtra("whichurl", which);
//									i.putExtra("imagesCount",
//											listImageView.size());
//									i.putExtra("cacheThumbnail",
//											asyncBitmapLoader.fileCache);
//									activity.startActivity(i);

								}

							});
				}

			}

		} else {
			v = inflater.inflate(R.layout.log_list_date_item, null);
			TextView data = (TextView) v.findViewById(R.id.set_date);
			data.setText(groupkey);

		}

		return v;
	}

	public void addNewsItem(List<ModleTextandView> dataArray) {
		for (ModleTextandView _item : dataArray) {
			ImageAndText.add(_item);
		}

	}

	public void removeNewsItem(List<ModleTextandView> dataArray) {
		for (ModleTextandView _item : dataArray) {
			ImageAndText.remove(_item);
		}

	}

}
