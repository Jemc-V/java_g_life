package com.gpluslife.home.log.unit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.gpluslife.R;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPUtils;

public class BitmapLoaders {

	MemoryCache memoryCache = new MemoryCache();
	public IputToFile fileCache;
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	ExecutorService executorService;
	Handler handler = new Handler();// handler to display images in UI thread

	public BitmapLoaders(Context context) {
		fileCache = new IputToFile(context);
		executorService = Executors.newFixedThreadPool(10);
	}

	final int stub_id = R.drawable.ic_launcher;

	public void DisplayImage(final ImageView imageView, final String sesionid,
			final String logid, final String imageURL) {

		imageViews.put(imageView, imageURL);
		Bitmap bitmap = memoryCache.get(imageURL);

		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
		} else {
			queuePhoto(sesionid, logid, imageURL, imageView);
			imageView.setImageResource(stub_id);
		}

	}

	public Bitmap getSmallFile(String imageURL) {
		Bitmap b = null;
		try {
			b = fileCache.getFile(imageURL);
		} catch (FileNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return b;

	}

	/*
	 * 启动线程下载
	 */

	private void queuePhoto(String sesionid, String logid, String imageURL,
			ImageView imageView) {
		// TODO 自动生成的方法存根
		PhotoToLoad p = new PhotoToLoad(sesionid, logid, imageURL, imageView);
		executorService.submit(new PhotosLoader(p));
	}

	Bitmap bmp = null;

	private Bitmap getBitmap(final String sesionid, final String logid,
			final String imageURL) {
		// TODO 自动生成的方法存根

		// from SD cache
		Bitmap b;
		try {
			b = fileCache.getFile(imageURL);
			if (b != null)
				return b;
		} catch (FileNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		try {
			GPRequestService.get().downlaodLogThumbnail(sesionid, logid, imageURL,
					new GPClientCallBack() {
						@SuppressLint("NewApi")
						@Override
						public void onSuccess(String jsonString)
								throws Exception {
							Map<String, Object> map = GPUtils
									.getMapForJson(jsonString);
							int code = Integer.parseInt(map.get("code")
									.toString());
							Log.d("获取随手记图片code返回值", "code=" + code);
							if (code == 200) {
								/*
								 * 当code=200时就可以从数据库内获取数据图片
								 */

								String result = map.get("result").toString();
								Log.d("result", result.toString());
								Map<String, Object> values = GPUtils
										.getMapForJson(result);
								Log.d("values", values.toString());
								String picbase64 = values.get("base64")
										.toString();

								final byte[] bitmapArray;
								bitmapArray = Base64.decode(picbase64,
										Base64.DEFAULT);

								BitmapFactory.Options options = new BitmapFactory.Options();
								// options.inJustDecodeBounds=true;
								// Bitmap
								// bitmapfactory=BitmapFactory.decodeByteArray(bitmapArray,
								// 0, bitmapArray.length);
								// options.inJustDecodeBounds=false;
								// int be =(int)(options.outHeight);
								// if(be<=0) be =4;
								// options.inSampleSize=be;
								// Bitmap
								// bitmap=BitmapFactory.decodeByteArray(bitmapArray,
								// 0, bitmapArray.length,options);
								Bitmap bitmap = BitmapFactory.decodeByteArray(
										bitmapArray, 0, bitmapArray.length,
										options);
								/*
								 * 获取到图片并压缩裁剪
								 */

								// int x = bitmap.getWidth() /4;
								// int y = bitmap.getHeight()/4;
								// int IMAGE_WIDTH = bitmap.getWidth() /2;
								// int IMAGE_HEIGHT = bitmap.getWidth() /2;
								//
								// Log.d("image width", bitmap.getWidth() + "" +
								// "====" + bitmap.getHeight());
								// Bitmap bitmaps=Bitmap.createBitmap(bitmap,
								// x,y, IMAGE_WIDTH, IMAGE_HEIGHT) ;

								/*
								 * 创建文件夹
								 */

								fileCache.setToSmallFile(imageURL, bitmap);
								bmp = fileCache.getFile(imageURL);

							}
						}

					});
			return bmp;
		} catch (Throwable ex) {
			ex.printStackTrace();
			if (ex instanceof OutOfMemoryError)
				memoryCache.clear();
			return null;
		}

	}

	public class PhotoToLoad {
		public String imageURL;
		public ImageView imageView;
		public String sesionid;
		public String logid;

		public PhotoToLoad(String s, String l, String u, ImageView i) {
			sesionid = s;
			logid = l;
			imageURL = u;
			imageView = i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			try {
				if (imageViewReused(photoToLoad))
					return;
				Bitmap bmp = getBitmap(photoToLoad.sesionid, photoToLoad.logid,
						photoToLoad.imageURL);
				memoryCache.put(photoToLoad.imageURL, bmp);

				if (imageViewReused(photoToLoad))
					return;
				BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
				handler.post(bd);
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
	}

	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		@Override
		public void run() {
			// TODO 自动生成的方法存根
			if (imageViewReused(photoToLoad))
				return;

			if (bitmap != null)

				photoToLoad.imageView.setImageBitmap(bitmap);
			else
				photoToLoad.imageView.setImageResource(stub_id);
		}
	}

	public boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.imageURL))
			return true;
		return false;
	}

}
