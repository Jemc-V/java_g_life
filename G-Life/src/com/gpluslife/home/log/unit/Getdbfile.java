package com.gpluslife.home.log.unit;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gpluslife.database.GPDatabaseActivity;
import com.gpluslife.database.GPDatabaseManger;
import com.gpluslife.modle.GPLogModle;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;



public class Getdbfile {
	private static final String TAG = "Getdbfile";
	ModleTextandView TextAndView;
	// public List<ModleTextandView> dataAarray = new
	// ArrayList<ModleTextandView>();
	 SQLiteDatabase sqlitedb;
	GPDatabaseActivity dbhelp;
	public Activity contexts;
	private List<GPLogModle> data;
	private String picttureId;

	public Getdbfile(Activity context) {
		this.contexts = context;
		dbhelp = new GPDatabaseActivity(contexts);
		sqlitedb = dbhelp.getWritableDatabase();
	}

	public List<ModleTextandView> getModleId(int page) {

		data = GPDatabaseManger.getLogDataBy(contexts, page);

		if (data != null) {
			List<ModleTextandView> dataAarray = new ArrayList<ModleTextandView>();
			if (data.size() > 0) {
				System.out.println("LogDbManager==============》》》"  +"数据长度"+ data.size() + "LogDbManager数据=========>>"+data.toString());
				List<Object> models = filterList(data);
				for (int i = 0; i < models.size(); i++) {

					System.out.println("LogDbManager" + "models"
							+ models.size() + "");
					if (models.get(i) instanceof String) {
						String alltime0 = null;
						String appId = null, appName = null, creatTime = null, content = null, isNote = null, logId = null, module = null;
						List<String> list = new ArrayList<String>();
						alltime0 = (String) models.get(i);
						TextAndView = new ModleTextandView(appId, appName,
								creatTime, content, isNote, logId, list,
								module, alltime0);
						Log.d(TAG, "ALLTIME=" + alltime0 + "APP_ID=" + appId
								+ "APPNAME=" + appName + "CREATETIME"
								+ creatTime + "CONTENT=" + content + "ISNOTE="
								+ isNote + "LOGID=" + logId + "MODULE="
								+ module);

						dataAarray.add(TextAndView);
					} else {
						String alltime = null;
						String appId = null, appName = null, creatTime = null, content = null, isNote = null, logId = null, module = null;
						List<String> list = new ArrayList<String>();
						Object model = models.get(i);
						GPLogModle m = (GPLogModle) model;
						String pictures = m.PICTURES;
						JSONArray jsonArray = null;
						try {
							JSONObject jsonObject;
							jsonArray = new JSONArray(pictures);
							for (int j = 0; j < jsonArray.length(); j++) {
								jsonObject = jsonArray.getJSONObject(j);
								picttureId = jsonObject.getString("pictureId");
								list.add(picttureId);
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						appId = m.APP_ID;
						appName = m.APPNAME;
						creatTime = m.CREATETIME;
						content = m.CONTENT;
						isNote = m.ISNOTE;
						logId = m.LOGID;
						module = m.MODULE;

						TextAndView = new ModleTextandView(appId, appName,
								creatTime, content, isNote, logId, list,
								module, alltime);
						dataAarray.add(TextAndView);
						Log.d(TAG, "pic=" + list + "ALLTIME=" + alltime
								+ "APP_ID=" + appId + "APPNAME=" + appName
								+ "CREATETIME" + creatTime + "CONTENT="
								+ content + "ISNOTE=" + isNote + "LOGID="
								+ logId + "MODULE=" + module);

					}
				}

				System.out.println("LogDbManager" + "dataAarray"
						+ dataAarray.size() + "");
				return dataAarray;

			}
		}
		return null;
	}

	private List<Object> filterList(List<GPLogModle> list) {
		List<Object> models = new ArrayList<Object>();
		List<String> times = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {

			GPLogModle model = list.get(i);
			if (i == 0) {
				String time = model.CREATETIME;
				String date = time.substring(0, 11);
				models.add(date);
				models.add(model);
				times.add(date);
			} else {
				// String preDate = models.get(0).toString();

				String newDate = model.CREATETIME.substring(0, 11);
				if (times.contains(newDate))
				// if(preDate.equals(newDate))
				{
					models.add(model);
				} else {
					String tempTime = model.CREATETIME.substring(0, 11);
					times.add(tempTime);
					models.add(tempTime);
					models.add(model);
				}
			}
		}

		return models;
	}
}
