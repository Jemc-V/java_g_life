package com.gpluslife.home.log.unit;

import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import com.gpluslife.database.GPDatabaseActivity;
import com.gpluslife.modle.GPLogModle;
import com.gpluslife.service.GPUserService;
import com.gpluslife.service.http.GPClientCallBack;

import android.R.integer;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;



public class GetServiceToDb {

	public String TAG = "GetServiceToDb";
	public FragmentActivity Activity;
	private String startday;
	SQLiteDatabase sqlitedb;
	private String phone;
	ModleTextandView TextAndView;
	public GetServiceToDb(FragmentActivity activity) {
		this.Activity = activity;
		GPDatabaseActivity	 dbhelp = new GPDatabaseActivity(Activity);
		sqlitedb = dbhelp.getWritableDatabase();
	}

	private static String convertDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(date);
		return dateString;
	}
	
//	
//	public List<ModleTextandView> ServertoAdapter(String sessionId,String pageNum){
//		List<ModleTextandView> dataAarray = new ArrayList<ModleTextandView>();
//		String pageSize = String.valueOf(20);
//		GPUserService.get().getmyLog(sessionId, pageNum,
//				pageSize, new GPClientCallBack() {
//			
//			@SuppressLint("NewApi")
//			public void onSuccess(String jsonString) throws Exception {
//				System.out.println("查看服务器的JSON数据：" + jsonString);
//				JSONObject jsonObj = new JSONObject(jsonString);
//				int code = Integer.parseInt(jsonObj.getString("code"));
//				if (code == 200) {
//						
//					List<GPLogModle> models = GPLogModle.initModel(jsonObj);
//					ContentValues contentValues = new ContentValues();
//					for (GPLogModle values : models) {
//						System.out.println("查看数据表是否成立"+"he ====>where值=="+models.size());
//						contentValues.put(
//								GPDatabaseActivity.CREATETIME,
//								values.CREATETIME);
//						contentValues.put(
//								GPDatabaseActivity.CONTENT,
//								values.CONTENT);
//						contentValues.put(
//								GPDatabaseActivity.LOGID,
//								values.LOGID);
//						contentValues.put(
//								GPDatabaseActivity.ISNOTE,
//								values.ISNOTE);
//						contentValues.put(
//								GPDatabaseActivity.PICTURES,
//								values.PICTURES);
//						
//						
//						TextAndView = new ModleTextandView(values.APP_ID, values.APPNAME,
//								values.CREATETIME, values.CONTENT, values.ISNOTE, values.LOGID, list,
//								values.MODULE, values.);
//					}
//				}
//			}
//		});
//		return dataAarray;
//		
//	}
	
	
	
	

	public void WriteToDb(String sessionId,String pageNum) {	
		/*
		 * 从服务器获取数据
		 */	
		String pageSize = String.valueOf(20);
		
		System.out.println("查看发到服务器的JSON数据：" + sessionId + pageNum
				);
		GPUserService.get().getmyLog(sessionId, pageNum,
				pageSize, new GPClientCallBack() {
					@SuppressLint("NewApi")
					public void onSuccess(String jsonString) throws Exception {
						System.out.println("查看服务器的JSON数据：" + jsonString);
						JSONObject jsonObj = new JSONObject(jsonString);
						int code = Integer.parseInt(jsonObj.getString("code"));
						if (code == 200) {
								
							List<GPLogModle> models = GPLogModle.initModel(jsonObj);
							ContentValues contentValues = new ContentValues();
							for (GPLogModle values : models) {
								System.out.println("查看数据表是否成立"+"he ====>where值=="+models.size());
								contentValues.put(
										GPDatabaseActivity.CREATETIME,
										values.CREATETIME);
								contentValues.put(
										GPDatabaseActivity.CONTENT,
										values.CONTENT);
								contentValues.put(
										GPDatabaseActivity.LOGID,
										values.LOGID);
								contentValues.put(
										GPDatabaseActivity.ISNOTE,
										values.ISNOTE);
								contentValues.put(
										GPDatabaseActivity.PICTURES,
										values.PICTURES);

								String where = values.LOGID;

					System.out.println("查看数据表是否成立"+"he ====>where值=="+where)		;	
								String selection = "logid =? ";
								String[] selections = new String[] { where };
								Cursor c = sqlitedb.query(GPDatabaseActivity.TABLE_NAME,
										null, selection, selections, null,
										null, null);

								if (!c.moveToNext()) {

									
									System.out
											.println("GetServieceTOdb查询不相同的logid数据插入"
												);

									sqlitedb.insert(GPDatabaseActivity.TABLE_NAME, null,
											contentValues);
								} else {
									System.out
											.println("GetServieceTOdb查询是否有相同的logid数据"
													+ c.getColumnIndex("logid"));
								}

							}
						} else if (code == 102001) {
							Log.d(TAG, "No data !");
							Looper.prepare();
							Toast.makeText(Activity, "没有数据!", 500).show();
							Looper.loop();
						} else {
							Looper.prepare();
							Toast.makeText(Activity, "获取数据失败", 500).show();
							Looper.loop();
						}
					}
				});
	}

}
