package com.gpluslife.home.log.unit;

import java.util.List;

public class ModleTextandView {

	public String ID = "_id";
	public String APP_ID = "app_id";
	public String APPNAME = "app_name";
	public String CREATETIME = "time";
	public String CONTENT = "content";
	public String ISNOTE = "isnote";
	public String LOGID = "logid";
	public List<String> PICTURES;
	public String MODULE = "module";
	public String ALLTIME = "alltime";

	public ModleTextandView(String appId, String appName, String creatTime,
			String content, String isNote, String logId, List<String> pics,
			String module, String alltime) {
		this.APP_ID = appId;
		this.APPNAME = appName;
		this.CREATETIME = creatTime;
		this.CONTENT = content;
		this.ISNOTE = isNote;
		this.LOGID = logId;
		this.PICTURES = pics;
		this.MODULE = module;
		this.ALLTIME = alltime;

	}

	public void setALLTIME(String aLLTIME) {
		ALLTIME = aLLTIME;
	}

	public String getALLTIME() {
		return ALLTIME;

	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getAPP_ID() {
		return APP_ID;
	}

	public void setAPP_ID(String aPP_ID) {
		APP_ID = aPP_ID;
	}

	public String getAPPNAME() {
		return APPNAME;
	}

	public void setAPPNAME(String aPPNAME) {
		APPNAME = aPPNAME;
	}

	public String getCREATETIME() {
		return CREATETIME;
	}

	public void setCREATETIME(String cREATETIME) {
		CREATETIME = cREATETIME;
	}

	public String getCONTENT() {
		return CONTENT;
	}

	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}

	public String getISNOTE() {
		return ISNOTE;
	}

	public void setISNOTE(String iSNOTE) {
		ISNOTE = iSNOTE;
	}

	public String getLOGID() {
		return LOGID;
	}

	public void setLOGID(String lOGID) {
		LOGID = lOGID;
	}

	public List<String> getPICTURES() {
		return PICTURES;
	}

	public void setPICTURES(List<String> pICTURES) {
		PICTURES = pICTURES;
	}

	public String getMODULE() {
		return MODULE;
	}

	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}

}
