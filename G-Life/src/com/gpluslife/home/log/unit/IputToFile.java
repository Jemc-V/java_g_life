package com.gpluslife.home.log.unit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class IputToFile implements Serializable {

	private File File1, File2;
	private String phone;
	private String path = "/mnt/sdcard/Gplus/";

	public IputToFile(Context context) {
		SharedPreferences sp = context.getSharedPreferences("sessionId",
				context.MODE_WORLD_READABLE);
		phone = sp.getString("username", phone);

		File1 = new File(path + phone + "/LazyList/BigImage/");
		File2 = new File(path + phone + "/LazyList/SmallImage/");

		if (!File1.exists() && !File2.exists()) {
			File1.mkdirs();
			File2.mkdirs();
		}
	}

	public Bitmap getFile(String imageURL) throws FileNotFoundException,
			IOException {

		String bitmapName = "/" + imageURL + "_thumb" + ".jpg";

		Log.d("save thumb image  name",
				bitmapName + BitmapFactory.decodeFile(File2 + bitmapName));
		return BitmapFactory.decodeFile(File2 + bitmapName);

	}

	public Bitmap getBigFile(String imageURL) throws FileNotFoundException,
			IOException {
		String bitmapName = "/" + imageURL + ".jpg";

		Log.d("save thumb image  name",
				bitmapName + BitmapFactory.decodeFile(File1 + bitmapName));
		return BitmapFactory.decodeFile(File1 + bitmapName);

	}

	public void setToFile(String imageURL, Bitmap bitmap) throws IOException {

		File imagefile = new File(File1, "/" + imageURL + ".jpg");
		imagefile.createNewFile();
		FileOutputStream fos = new FileOutputStream(imagefile);
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

	}

	public void setToSmallFile(String imageURL, Bitmap bitmap)
			throws IOException {

		File thumbImageFile = new File(File2, "/" + imageURL + "_thumb"
				+ ".jpg");
		thumbImageFile.createNewFile();
		FileOutputStream thumbfos = new FileOutputStream(thumbImageFile);
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, thumbfos);

	}

}
