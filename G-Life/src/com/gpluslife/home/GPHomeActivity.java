package com.gpluslife.home;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Map;
import java.util.zip.Inflater;

import com.gpluslife.R;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.customView.SegmentView;
import com.gpluslife.customView.SegmentView.onSegmentViewClickListener;
import com.gpluslife.drawer.LeftSlidingMenuFragment;
import com.gpluslife.filemanager.GPFileManager;
import com.gpluslife.home.Qr_code.GPQrCodeActivity;
import com.gpluslife.home.account.GPFamilyMemberActivity;
import com.gpluslife.home.fragment.GPHomeFragment;
import com.gpluslife.home.fragment.GPLogFragment;
import com.gpluslife.home.note.NoteActivity;
import com.gpluslife.home.plugin.GPDeviceMangementActivity;
import com.gpluslife.home.plugin.management.GPPluginJSONParser;
import com.gpluslife.login.GPLoginActivity;
import com.gpluslife.login.GPLoginLoadingDialog;
import com.gpluslife.login.model.GPLoginUserModel;
import com.gpluslife.login.model.GPUserModelManager;
import com.gpluslife.profile.GPUserProfileActivity;
import com.gpluslife.service.GPRequestService;
import com.gpluslife.service.http.GPClientCallBack;
import com.gpluslife.utils.GPBitmapUtils;
import com.gpluslife.utils.GPConstants;
import com.gpluslife.utils.GPErrorStlye;
import com.gpluslife.utils.GPUtils;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

//import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import android.R.attr;
import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

@SuppressLint("NewApi") public class GPHomeActivity extends SlidingFragmentActivity implements OnClickListener {
	
	public static final String HOME_TAG = "GPHomeActivity";
	
	private TextView homeTextView ;
	private TextView logTextView; 
	private FragmentManager fragmentManager;
	
	private GPHomeFragment homeFragment;
	private GPLogFragment 	logFragment;
	
	private GPNavigationView navigationView;
	private Button rightButton;
	private Button leftButton;
	
	private GPSelectDialogView dialogView;
	private int dialogViewXML;
	
	protected SlidingMenu mSlidingMenu;
	
	private Handler mHandler;
	
	public static final int HOMETEXTVIEW_ID  =  1000;
	public static final int LOGTEXTVIEW_ID =  1001;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		
		
		mHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				Bundle bundle = msg.getData();
				byte[] iconByte = bundle.getByteArray("iconByte");
				Bitmap iconBitmap = BitmapFactory.decodeByteArray(iconByte, 0, iconByte.length);
				@SuppressWarnings("deprecation")
				Drawable drawable = new BitmapDrawable(iconBitmap);
				leftButton.setBackground(drawable);		
				leftButton.invalidate();
				iconBitmap = null;
				iconByte = null;
				drawable = null;
			}
		};
		initActivity();		
		initSlidingMenu();
		downloadHeadIcon();
		
	}
	
	private void initSlidingMenu() {

		// TODO Auto-generated method stub
		
		if (fragmentManager == null)
		{
			fragmentManager = getSupportFragmentManager();
		}
	
		setBehindContentView(R.layout.main_left_layout);// 锟斤拷锟斤拷台锟斤拷锟斤拷锟斤拷为锟秸碉拷framelayout
//		FragmentTransaction mFragementTransaction = getSupportFragmentManager()
//				.beginTransaction();
		Fragment mFrag = new LeftSlidingMenuFragment();
		fragmentManager.beginTransaction().replace(R.id.main_left_fragment, mFrag);// 锟斤拷锟斤拷台锟斤拷framelayout锟芥换为slidingmenu
		fragmentManager.beginTransaction().commit();
		mSlidingMenu = getSlidingMenu();
		mSlidingMenu.setMode(SlidingMenu.LEFT);
		mSlidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		mSlidingMenu.setFadeDegree(0.35f);
		mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
//		mSlidingMenu.setShadowDrawable(R.drawable.shadow);
		mSlidingMenu.setFadeEnabled(true);
		mSlidingMenu.setBehindScrollScale(0.333f);
		
	}
	
	private void initActivity()
	{
		if (fragmentManager == null)
		{
			fragmentManager = getSupportFragmentManager();
		}
	
		if (homeFragment == null)
		{
			homeFragment = new GPHomeFragment();
		}
	
		fragmentManager.beginTransaction().add(R.id.content_fragment, homeFragment).commit();
		
		navigationView = (GPNavigationView)findViewById(R.id.navigationView);
		navigationView.setLeft_Button_drawable(R.drawable.home_swop);
		
		rightButton   = navigationView.getBtn_right();
		leftButton 		= navigationView.getBtn_left();
		rightButton.setOnClickListener(this);
		leftButton.setOnClickListener(this);
		
		SegmentView segmentView = navigationView.getSegmentView();
		 homeTextView = segmentView.getTextView1();
		 homeTextView.setId(HOMETEXTVIEW_ID);
		 logTextView = segmentView.getTextView2();
		 logTextView.setId(LOGTEXTVIEW_ID);
		 homeTextView.setOnClickListener(this);
		 logTextView.setOnClickListener(this);
	}

	// 切换home与log界面
	private void initFragment(String type)
	{
		if(type.equals("log"))
		{
			if (logFragment == null )
			{
				logFragment = new GPLogFragment();
			}
			fragmentManager.beginTransaction().replace(R.id.content_fragment, logFragment).commit();
		}
		else if(type.equals("home"))
		{
			if (homeFragment == null)
			{
				homeFragment = new GPHomeFragment();
			}
			fragmentManager.beginTransaction().replace(R.id.content_fragment,homeFragment).commit();
		}

	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int vId = v.getId();
		switch (vId) {
		case HOMETEXTVIEW_ID:
		{
			if (homeTextView.isSelected()) {
				return;
			}
			homeTextView.setSelected(true);
			logTextView.setSelected(false);
			Log.e( HOME_TAG,"homeTextView"  );
			initFragment("home");
		
		}
			break;
		case LOGTEXTVIEW_ID:
		{
			if (logTextView.isSelected())
			{
				return;
			}
			logTextView.setSelected(true);
			homeTextView.setSelected(false);
			
			Log.e( HOME_TAG,"logTextView"  );
			
			initFragment("log");
		}
			break;
		case GPConstants.NAVIGATION_RIGHT_BUTTON:
 {
			if (dialogView == null) {
				dialogView = new GPSelectDialogView(GPHomeActivity.this,
						R.style.dialog);

				Window win = dialogView.getWindow();
				LayoutParams params = new LayoutParams();
				params.x = 1000;// 设置x坐标
				params.y = -4000;// 设置y坐标
				win.setAttributes(params);

			}
			dialogView.setCanceledOnTouchOutside(true);// 设置点击Dialog外部任意区域关闭Dialog
			dialogView.show();

			TableRow logTableRow = dialogView.getLogTableRow();

			logTableRow.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent=new Intent().setClass(GPHomeActivity.this, NoteActivity.class);
					startActivity(intent);
					//finish();
					dialogView.dismiss();

				}
			});

			TableRow qRcodeTableRow = dialogView.getqRCodeTableRow();
			qRcodeTableRow.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent=new Intent().setClass(GPHomeActivity.this, GPQrCodeActivity.class);
					startActivity(intent);
				}
			});

			TableRow functionTableRow = dialogView.getFunctionTableRow();
			functionTableRow.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					Intent intent = new Intent(GPHomeActivity.this,
							GPDeviceMangementActivity.class);
					startActivity(intent);

				}
			});

			TableRow accountTableRow = dialogView.getAccountTableRow();
			final TextView accountTextView = (TextView) dialogView
					.findViewById(R.id.account);
			if (GPConstants.ISMAINHOST == false) {
				accountTextView.setText(R.string.GPLUS_FAMILY_MEMBER_EXIT);
			}
			accountTableRow.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					Log.d(HOME_TAG, accountTextView.getText().toString()
							+ " == " + getString(R.string.membermange));

					if (GPConstants.ISMAINHOST == false)
					{
						String deviceKey = Secure.getString(
								GPHomeActivity.this.getContentResolver(),
								Secure.ANDROID_ID);
						final GPLoginUserModel userModel = GPUserModelManager
								.getUserModelFromSharePreferences(GPHomeActivity.this, true);

						if (userModel.getDeviceKey() != null && userModel.getDeviceKey().equals(deviceKey)) 
						{
							GPConstants.ISMAINHOST = true;
							GPUserModelManager.setIsMainHost(GPHomeActivity.this,	true);
							GPLoginUserModel.setModel(userModel);
							accountTextView.setText(R.string.membermange);
							GPUserModelManager.destroyUserModel(	GPHomeActivity.this,"tempusermodel");
							String iconName = "HEADICON";
							String iconPathString =GPFileManager.getFileManager().createFilePath(null);
							iconPathString += File.separator + iconName;
							Bitmap iconBitmap = GPBitmapUtils.getBitmapFromDirectory(iconPathString);;
							Drawable drawable = new BitmapDrawable(iconBitmap);
							leftButton.setBackground(drawable);		
							leftButton.invalidate();
							iconBitmap = null;
							drawable = null;
						} 
						else
						{
							final GPLoginLoadingDialog loadingDialog = new GPLoginLoadingDialog(
									GPHomeActivity.this);
							loadingDialog.show();

							GPRequestService.get().login(	userModel.getUserName(),	userModel.getPassword(), deviceKey,
									new GPClientCallBack() {

										@Override
										public void onSuccess(String result)
												throws Exception {
											// TODO Auto-generated method stub
											super.onSuccess(result);

											Map<String, Object> map = GPUtils.getMapForJson(result);

											int code = 0;
											if (map.containsKey("code"))
											{
												code = Integer.parseInt(map.get("code").toString());
											}
											if (code == 200) {
												GPConstants.ISMAINHOST = true;
												GPUserModelManager.setIsMainHost(GPHomeActivity.this,	true);
												GPUserModelManager.destroyUserModel(GPHomeActivity.this,"tempusermodel");
												if (map.containsKey("result")) {
													String resultString = map.get("result").toString();
													GPLoginUserModel tempUserModel = GPUserModelManager.parserJsonString(GPHomeActivity.this,resultString,userModel.getUserName(),true);

													GPUserModelManager.saveUserModelToSharePreferences(GPHomeActivity.this,tempUserModel,	tempUserModel.getPassword());
													loadingDialog.dismiss();
													accountTextView.setText(R.string.membermange);
													Looper.prepare();
													Toast.makeText(	GPHomeActivity.this,"登录成功！",Toast.LENGTH_SHORT).show();
													Looper.loop();
												}
											} 
											else
											{
												loadingDialog.dismiss();
												GPErrorStlye.stlye(	GPHomeActivity.this,code);
											}
										}
										@Override
										public void onError(Throwable ex) {
											// TODO Auto-generated method stub
											super.onError(ex);
											loadingDialog.dismiss();
										}
									});
						}

					} 
					else if (GPConstants.ISMAINHOST) 
					{
						Intent intent = new Intent(GPHomeActivity.this,
								GPFamilyMemberActivity.class);
						startActivityForResult(intent, 100);
					}
				}
			});
		}
			break;
		case GPConstants.NAVIGATION_LEFT_BUTTON:
		{
			Intent intent = new Intent(GPHomeActivity.this,GPUserProfileActivity.class);
			startActivity(intent);
			finish();
		}
			break;
		default:
			break;
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == 200)
		{
			downloadHeadIcon();
			GPConstants.ISMAINHOST  = false;
			GPUserModelManager.setIsMainHost(GPHomeActivity.this, false);
			TextView accountTextView = (TextView)dialogView.findViewById(R.id.account);
			accountTextView.setText(R.string.GPLUS_FAMILY_MEMBER_EXIT);
		}
		
	}
	
    public static int px2dip(Context context, float pxValue) {  
        final float scale = context.getResources().getDisplayMetrics().density;  
        return (int) (pxValue / scale + 0.5f);  
    }  
	
	private void downloadHeadIcon()
	{
		GPLoginUserModel userModel = GPLoginUserModel.get();
		if (userModel.getIsDownloadIcon() != null && !userModel.getIsDownloadIcon())
		{
			String iconName = "HEADICON";
			String iconPathString =GPFileManager.getFileManager().createFilePath(null);
			iconPathString += File.separator + iconName;
			Bitmap iconBitmap = GPBitmapUtils.getBitmapFromDirectory(iconPathString);;
			Drawable drawable = new BitmapDrawable(iconBitmap);
			leftButton.setBackground(drawable);		
			leftButton.invalidate();
			iconBitmap = null;
			drawable = null;
			/*
			Runnable runnable = new Runnable() {				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					String iconName = "HEADICON";
					String iconPathString =GPFileManager.getFileManager().createFilePath(iconName);
					 byte[] bitmapByte = GPBitmapUtils.getBitmapByteFromDirectory(iconPathString);;
					Bundle bundle = new Bundle();
					bundle.putByteArray("iconByte", bitmapByte);
					Message msg = new Message();
					msg.setData(bundle);
					mHandler.sendMessage(msg);
					bitmapByte = null;
				}
			};
			
			new Thread(runnable).start();
			*/
		}
		else {
			
			GPRequestService.get().download(userModel.getSessionId(), userModel.getAccountName(), 
					new GPClientCallBack()
			{
				@Override
				public void onSuccess(String result) throws Exception {
					// TODO Auto-generated method stub
					super.onSuccess(result);
					
					Map<String, Object> resultMap = GPUtils.getMapForJson(result);
					Log.d(HOME_TAG, " down icon ===>" + resultMap.toString());
					int code = Integer.parseInt(resultMap.get("code").toString());
					if (code == 200)
					{
						String iconString = resultMap.get("result").toString();
						Map<String, Object> iconMap = GPUtils.getMapForJson(iconString);
						String basesString = iconMap.get("base64").toString();
						Bitmap headBitmap = GPBitmapUtils.stringtoBitmap(basesString);
						Bitmap iconBitmap= GPBitmapUtils.toRoundBitmap(headBitmap);
						 ByteArrayOutputStream baos = new ByteArrayOutputStream();
						 iconBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
						 byte[] bitmapByte = baos.toByteArray();
						 baos.close();
						Bundle bundle = new Bundle();
						bundle.putByteArray("iconByte", bitmapByte);
						Message msg = new Message();
						msg.setData(bundle);
						mHandler.sendMessage(msg);
						
						headBitmap.recycle();
						headBitmap = null;
						bitmapByte = null;
						
						String iconPath = GPFileManager.getFileManager().createFilePath(null);
						iconPath += File.separator + "HEADICON";
						GPBitmapUtils.saveImgToDirectory(iconBitmap, iconPath);
						iconBitmap.recycle();
						iconBitmap = null;
					}
					
				}
			});
		}
	
	}
	



}
