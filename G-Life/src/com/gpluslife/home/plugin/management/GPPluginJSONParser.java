package com.gpluslife.home.plugin.management;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.gpluslife.home.plugin.model.GPPluginModel;
import com.gpluslife.utils.GPUtils;

@SuppressLint("NewApi") public class GPPluginJSONParser
{
	
	private static String TAG = "GPPluginJSONParser";
	
	 private static String readFile(String fileName,Activity activity) throws IOException{
         ByteArrayOutputStream outputStream = new ByteArrayOutputStream(1024);
//         FileInputStream inputStream = new FileInputStream(fileName);
         InputStream inputStream = activity.getAssets().open(fileName);
         int len = 0;
         byte[] buffer = new byte[1024];
         while((len = inputStream.read(buffer)) != -1){
                 outputStream.write(buffer, 0, len);
         }
         outputStream.close();
         inputStream.close();
         byte[] data = outputStream.toByteArray();
         return new String(data);
	 }
	 
	 public static void parserJsonString(Activity activity )
	 {		 
		 String jsonString = null;
		try {
			jsonString = GPPluginJSONParser.readFile("plugin.json",activity);
			Log.d(TAG,jsonString);
			 JSONArray jsonArray = new JSONArray(jsonString);
			 Log.d(TAG,jsonArray.toString());			 
			int size =  jsonArray.length();
			 Log.d(TAG , "size = " + size);	
			for (int i = 0; i <size; i++)
			{
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Map<String, Object> map = GPUtils.getMapFromJsonObject(jsonObject);
				
				GPPluginModel pluginModel = new GPPluginModel(map);
				
				Log.d(TAG , "pluginModel = " + pluginModel.getPluginId() + pluginModel.getPluginName());	
				
				SharedPreferences sp = activity.getSharedPreferences(pluginModel.getPluginId(),
						activity.MODE_PRIVATE);
				Log.d(TAG , "sp = " + sp);	
				if (sp.getString("pluginId", null) == null)
				{
	
					Editor editor = sp.edit();
					
					editor.putString("pluginId", pluginModel.getPluginId());
					editor.putString("pluginName", pluginModel.getPluginName());
					editor.putString("pluginThumb", pluginModel.getPluginThumb());
					editor.putString("pluginSetting", pluginModel.getPluginSetting());
					editor.putStringSet("function", pluginModel.getFunctions());
					editor.putBoolean("isCreateLog", true);
//					editor.putBoolean("isSelected", false);
					editor.putStringSet("functionImage", pluginModel.getFunctionImages());
					editor.putStringSet("functionClass", pluginModel.getFunctionClasses());
					editor.putString("functionIsOpen", pluginModel.getFunctionIsOpens());
					editor.commit();
				}

			}
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 if (null == jsonString)
		 {
			 return ;
		}
	 
	 }
	 
	 public  static void updateIsOpen(Boolean isOpen,String pluginId,Activity activity,int position)
	 {
		 SharedPreferences sp = activity.getSharedPreferences(pluginId,
					activity.MODE_PRIVATE);
		 
		 String set = sp.getString("functionIsOpen", null);
		
		 if (set != null)
		 {
			 JSONArray jsonArray;
			try {
				jsonArray = new JSONArray(set);
				int length = jsonArray.length();
				
				if(length >= position)
				{
					jsonArray.put(position, isOpen);
					Editor editor = sp.edit();
				   editor.putString("functionIsOpen", jsonArray.toString());
				   editor.commit();
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 	
			 
//			 Object[] obj = set.toArray();
//			 int length = obj.length;
//			 if (length == 1)
//			 {
//				Boolean boolean1 =  Boolean.valueOf(obj[0].toString());
//				if (boolean1 != isOpen)
//				{
//					 if (position == 0)
//					 {
//						 tempSet.add(String.valueOf(isOpen));
//						tempSet.add(String.valueOf(boolean1));
//					}
//					 else {
//						
//							tempSet.add(String.valueOf(boolean1));
//							 tempSet.add(String.valueOf(isOpen));
//					}
//				}
//			}
//		
//			 
//			
//			 obj[position] = String.valueOf(isOpen);
//			
//			 
//			 for (int i = 0; i < length; i++)
//			 {
//				tempSet.add(obj[i].toString());
//			}
//			 Editor editor = sp.edit();
//			editor.putStringSet("functionIsOpen", tempSet);
//			editor.commit();
		}
	}
	
}
