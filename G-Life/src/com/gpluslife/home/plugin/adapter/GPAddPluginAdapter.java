package com.gpluslife.home.plugin.adapter;

import java.util.ArrayList;
import java.util.zip.Inflater;

import com.gpluslife.R;
import com.gpluslife.home.plugin.model.GPPluginModel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class GPAddPluginAdapter extends BaseAdapter  {
	
	private String TAG = "GPAddPluginAdapter";
	private ArrayList<GPPluginModel> pluginList  ;
	private Activity context;
	
	public GPAddPluginAdapter(Activity ctx)
	{
		context = ctx;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-gen erated method stub
		int count = pluginList.size();
		return count;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		GPPluginModel obj = pluginList.get(position);
		return obj;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("ViewHolder") @Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=View.inflate(context, R.layout.add_device_item, null);
		TextView pluginTextView = (TextView)convertView.findViewById(R.id.puls_name);
		ImageView iconView = (ImageView)convertView.findViewById(R.id.icon);
		
		ImageView addImageView = (ImageView)convertView.findViewById(R.id.add_btn);
		addImageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				GPPluginModel pluginModel = pluginList.get(position);
				Log.d(TAG, pluginModel.getPluginId());
				updatePluginState(true, pluginModel.getPluginId());
			}
		});
		
		GPPluginModel model = (GPPluginModel) getItem(position);
		if (model.getPluginName().equals("GPLUS_HOME_PLUGIN_BAND"))
		{
			pluginTextView.setText(R.string.GPLUS_HOME_PLUGIN_BAND);
			iconView.setImageResource(R.drawable.plugin_jordoan_icon);
		}
		else if (model.getPluginName().equals("GPLUS_HOME_PLUGIN_SCALE"))
		{
			pluginTextView.setText(R.string.GPLUS_HOME_PLUGIN_SCALE);
			iconView.setImageResource(R.drawable.plugin_platform_icon);
		}
		
		
		return convertView;
	}
	
	

	public void setPluginList(ArrayList<GPPluginModel> pluginList) {
		this.pluginList = pluginList;
	}

	private void updatePluginState(Boolean isSelected, String pluginId) 
	{
		Log.d(TAG, " pluginId = " + pluginId);
		SharedPreferences sp = context.getSharedPreferences(pluginId, context.MODE_PRIVATE); 
		Editor editor = sp.edit();
		editor.putBoolean("isSelected", isSelected);
		editor.commit();
		Log.d(TAG, " pluginId = " + sp.getBoolean("isSelected", false));
	}
	
}
