package com.gpluslife.home.plugin.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;

import com.google.gson.JsonArray;
import com.gpluslife.R;
import com.gpluslife.customView.GPSwitchButton;
import com.gpluslife.home.plugin.management.GPPluginJSONParser;
import com.gpluslife.home.plugin.model.GPPluginModel;
import com.gpluslife.utils.GPBitmapUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

public class GPDeviceManagementAdapter extends BaseExpandableListAdapter {

	private static String TAG = "GPDeviceManagementAdapter";
	
	private ArrayList<GPPluginModel> models;
	private LayoutInflater mInflater = null;
	private Activity mContext = null;
	
	public GPDeviceManagementAdapter(Activity activity)
	{
		mContext = activity;
		mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		
		return models.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		if (models.size() == 0) {
			return 0;
		}
		GPPluginModel model = models.get(groupPosition);
		int count = model.getFunctions().size();
		return count;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		if (models.size() == 0) {
			return null;
		}
		GPPluginModel model = models.get(groupPosition);
		return model;

	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		if (models.size() == 0) {
			return null;
		}
		GPPluginModel model = models.get(groupPosition);
		return model;
//		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = mInflater.inflate(R.layout.device_management, null);
		if(convertView != null)
		{
			GPPluginModel model = (GPPluginModel) getGroup(groupPosition);
			ImageView deviceIcon = (ImageView)convertView.findViewById(R.id.device_icon);
			
			TextView deviceNameTV = (TextView)convertView.findViewById(R.id.device_name);
			deviceNameTV.setText(model.getPluginName());
			
			Bitmap image = GPBitmapUtils.getImageFromAssetsFile(mContext, model.getPluginThumb()); 
			
				deviceIcon.setImageBitmap(image);
			
		}
		
		return convertView;
	}

	@SuppressLint("NewApi") @Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		convertView = mInflater.inflate(R.layout.device_manage_item, null);
		
		if (convertView != null)
		{
			TextView nameTextView = (TextView)convertView.findViewById(R.id.name);
			ImageView functionView = (ImageView)convertView.findViewById(R.id.icon);
			GPSwitchButton switchButton = (GPSwitchButton)convertView.findViewById(R.id.switchView);
			final GPPluginModel model = (GPPluginModel) getChild(groupPosition, childPosition);
			String isOpenString = model.getFunctionIsOpens();
			try {
				JSONArray jsonArray = new JSONArray(isOpenString);
				Boolean isOpen = jsonArray.getBoolean(childPosition);
				switchButton.setChecked(isOpen);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			Set<String> imageSet = model.getFunctionImages();
			Object[] imageObjects = imageSet.toArray() ;
			String imageName = (String) imageObjects[childPosition];
			Bitmap image = GPBitmapUtils.getImageFromAssetsFile(mContext, imageName); 
			functionView.setImageBitmap(image);
			
			Set<String> functionSet = model.getFunctions();
			Object[] functionObjects = functionSet.toArray() ;
			String functionString = (String) functionObjects[childPosition];
			nameTextView.setText(functionString);
			
			switchButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					
//					SharedPreferences sp = mContext.getSharedPreferences(model.getPluginId(), mContext.MODE_PRIVATE);
//				  	Set<String> isOpenSet = sp.getStringSet("functionIsOpen", null);
				  	GPPluginModel model = (GPPluginModel) getGroup(groupPosition);
				  	GPPluginJSONParser.updateIsOpen(isChecked, model.getPluginId(), mContext, childPosition);
				  	
//					if (isChecked == true)
//					{
//						
//					}
					
				}
			});
			
		}
		
		
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

	public void setModels(ArrayList<GPPluginModel> models) {
		
		this.models = models;
	}


}
