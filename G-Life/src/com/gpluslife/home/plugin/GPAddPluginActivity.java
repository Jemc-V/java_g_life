 package com.gpluslife.home.plugin;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

//import com.GrobalConstance;
import com.gpluslife.R;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.home.plugin.adapter.GPAddPluginAdapter;
import com.gpluslife.home.plugin.model.GPPluginModel;
import com.gpluslife.utils.GPConstants;
//import com.gpluslife.adapter.AddDeviceListAdapter;
//import com.gpluslife.utils.Download_adt;
//import com.gpluslife.utils.PreferencesUtils;
//import com.gpluslife.utils.SearchAdtUtils;

@SuppressLint("NewApi") public class GPAddPluginActivity extends Activity implements OnClickListener {
//	Download_adt  download_adt;
//	SearchAdtUtils  searchAdtUtils;
	private ImageView add_btn, returnlast;
	private ListView pluginListView;
	private GPNavigationView mNavigationView;
	private Button mBackButton;
	ArrayList<GPPluginModel> pluginsArrayList;
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context arg0, Intent intent) {
			String action = intent.getAction();
//			if (action.equals(MainActivity.ACTION_GATT_DISCONNECTED)) {
//				Toast.makeText(add_btn.getContext(), R.string.balcelet_connect_failed, Toast.LENGTH_LONG).show();
//			} else if (action.equals(MainActivity.ACTION_GATT_CONNECTED)) {
//				onDeviceConnneted();
//				finish();
//			} else if (action.equals(MainActivity.ACTION_GATT_CONNECTFAILED)) {
//				Toast.makeText(add_btn.getContext(), R.string.balcelet_connect_failed, Toast.LENGTH_LONG).show();
//			} else if (action
//					.equals(MainActivity.ACTION_GATT_SERVICES_DISCOVERED)) {
//				onDeviceConnneted();
//				finish();
//			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_device);
		
		mNavigationView = (GPNavigationView)findViewById(R.id.navigationView);
		mBackButton = mNavigationView.getBtn_left();
		mBackButton.setOnClickListener(this);
		
		pluginsArrayList = new ArrayList<GPPluginModel>();
		readPluginFromLocal();
		
//		ArrayList<String> pluginsArrayList = new ArrayList<String>();
//		pluginsArrayList.add("Jordan手环");
//		pluginsArrayList.add("磅秤");
//		pluginsArrayList.add("血糖仪");
		
		pluginListView = (ListView)findViewById(R.id.add_device_list);
		pluginListView.setItemsCanFocus(false);
		GPAddPluginAdapter adapter = new GPAddPluginAdapter(this);
		adapter.setPluginList(pluginsArrayList);
		pluginListView.setAdapter(adapter);

	}

	@Override
	public void onClick(View V) {
		switch (V.getId()) {
		case R.id.add_btn:
		{
			
		}
			break;
		case GPConstants.NAVIGATION_LEFT_BUTTON:
		{
			onBackPressed();
			finish();
		}
			break;
		default:
			break;
		}
	}

//	public void dialog() { 
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("      添加设备成功")  
//            .setIcon(R.drawable.device_settingbg3)  
//            .setCancelable(false)  
//            .setMessage("               是否立即绑定该设备")   
//            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Intent  SearchIntent = new Intent("com.gplus.blacelet.search");
//                    SearchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        			startActivity(SearchIntent);
//                	PreferencesUtils.putBoolean(AddDevc.this, GrobalConstance.GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED, true);
//                }
//            })
//            .setNegativeButton("取消", new DialogInterface.OnClickListener() {                    
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                	PreferencesUtils.putBoolean(AddDevc.this, GrobalConstance.GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED, true);
//                	Intent intent1 = new Intent();
//        			intent1.setClass(AddDevc.this, TabHostActivity.class);
//        			startActivity(intent1);
//        			finish();
//                }
//            }).show();
//	}
	
	@Override
	protected void onPause() {
		super.onPause();
//		IntentFilter mFilter = new IntentFilter();
//		mFilter.addAction(MainActivity.ACTION_GATT_DISCONNECTED);
//		mFilter.addAction(MainActivity.ACTION_GATT_SERVICES_DISCOVERED);
//		mFilter.addAction(MainActivity.ACTION_GATT_CONNECTED);
//		registerReceiver(mReceiver, mFilter);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
//		unregisterReceiver(mReceiver);
		
	}
	
	private void onDeviceConnneted(){
		setResult(Activity.RESULT_OK);
//		PreferencesUtils.putBoolean(this, GrobalConstance.GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED, true);
	}
	
	private void readPluginFromLocal()
	{
	 	SharedPreferences sp = this.getSharedPreferences("1000", Context.MODE_PRIVATE);
	 	Log.d("GPAddPluginActivity", "isSelected"  + sp.getBoolean("isSelected",false) + "") ;
	 	if (sp.getBoolean("isSelected",false) == false) 
	 	{
		 	GPPluginModel model = new GPPluginModel();
		 	model.setPluginId(sp.getString("pluginId", null));
		 	model.setPluginName(sp.getString("pluginName", null));
		 	model.setPluginSetting(sp.getString("pluginSetting", null));
		 	model.setPluginThumb(sp.getString("pluginThumb", null));
		 	model.setFunctions(sp.getStringSet("function", null));
		 	model.setFunctionClasses(sp.getStringSet("functionClass", null));
		 	model.setFunctionImages(sp.getStringSet("functionImage", null));
		 	model.setCreateLog(sp.getBoolean("isCreateLog", false));
		 	model.setSelected(sp.getBoolean("isSelected",false));
		 	pluginsArrayList.add(model);
		}

	 	
	 	SharedPreferences sp2 = this.getSharedPreferences("1002", Context.MODE_PRIVATE);
	 	if (sp2.getBoolean("isSelected",false) == false) 
	 	{
		 	GPPluginModel model2 = new GPPluginModel();
		 	model2.setPluginId(sp2.getString("pluginId", null));
		 	model2.setPluginName(sp2.getString("pluginName", null));
		 	model2.setPluginSetting(sp2.getString("pluginSetting", null));
		 	model2.setPluginThumb(sp2.getString("pluginThumb", null));
		 	model2.setFunctions(sp2.getStringSet("function", null));
		 	model2.setFunctionClasses(sp2.getStringSet("functionClass", null));
		 	model2.setFunctionImages(sp2.getStringSet("functionImage", null));
		 	model2.setCreateLog(sp.getBoolean("isCreateLog", false));
		 	model2.setSelected(sp.getBoolean("isSelected",false));
		 	pluginsArrayList.add(model2);
	 	}
	}
	
}
