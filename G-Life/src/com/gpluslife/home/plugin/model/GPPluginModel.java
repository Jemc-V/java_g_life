package com.gpluslife.home.plugin.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import com.gpluslife.utils.GPUtils;

import android.util.Log;

public class GPPluginModel {
	
	private static String TAG = "GPPluginModel";
	
	private String pluginId;
	private String pluginName;
	private String pluginThumb;
	private String pluginSetting;
	private Set<String> functions;
	private Set<String>  functionImages;
	private Set<String> functionClasses;
	private Set<String> databases;
	private Set<String> modules;
	private String	 functionIsOpens;
	private boolean isSelected;
	private boolean isCreateLog;
	
	public GPPluginModel( )
	{
		
	}
	
	public GPPluginModel(Map<String, Object> map)
	{
		
		String pluginIdString =map.get("pluginId").toString();
		String pluginNameString = map.get("plugin").toString();
		String pluginThumbString = map.get("pluginThumb").toString();
		String pluginSettingString = map.get("pluginSetting").toString();
//		Boolean iscreateLogBoolean = Boolean.parseBoolean(map.get("isCreateLog").toString()) ;
		
		Log.d(TAG, "pluginIdString = " + pluginIdString + "  " + pluginNameString  + "  " + pluginThumbString);
		
		Map<String, Object> functionMap = (Map<String, Object>) GPUtils.getMapFromJsonObject((JSONObject)map.get("function"));
		Map<String, Object> thumbMap = (Map<String, Object>) GPUtils.getMapFromJsonObject((JSONObject)map.get("thumb"));
		
		Map<String, Object> classMap = (Map<String, Object>) GPUtils.getMapFromJsonObject((JSONObject)map.get("class"));
		
		Log.d(TAG, "functionMap = " + functionMap.toString() + "  " + thumbMap  + "  " + classMap);
		
		int size = functionMap.size();
		Set<String> functionSet = new HashSet<String>();
		Set<String> thumbSet = new HashSet<String>();
		Set<String> classSet = new HashSet<String>();
		ArrayList<Boolean> isOpenSet = new ArrayList<Boolean>();
		for (int i = 0; i < size; i++) 
		{
			String key1 = "function" + String.valueOf(i+1);
			String key2 = "thumb" + String.valueOf(i+1);
			String key3 = "class" + String.valueOf(i+1);
			functionSet.add(functionMap.get(key1).toString());
			thumbSet.add(thumbMap.get(key2).toString());
			classSet.add(classMap.get(key3).toString());
			isOpenSet.add(true);
		}
		
		this.setPluginId(pluginIdString);
		this.setPluginName(pluginNameString);
		this.setPluginThumb(pluginThumbString);
		this.setPluginSetting(pluginSettingString);
		this.setCreateLog(true);
		this.setSelected(false);
		this.setFunctions(functionSet);
		this.setFunctionImages(thumbSet);
		this.setFunctionClasses(classSet);
		this.setFunctionIsOpens(isOpenSet.toString());
	}
	
	public String getPluginId() {
		return pluginId;
	}
	public void setPluginId(String pluginId) {
		this.pluginId = pluginId;
	}
	public String getPluginName() {
		return pluginName;
	}
	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}
	public String getPluginThumb() {
		return pluginThumb;
	}
	public void setPluginThumb(String pluginThumb) {
		this.pluginThumb = pluginThumb;
	}
	public String getPluginSetting() {
		return pluginSetting;
	}
	public void setPluginSetting(String pluginSetting) {
		this.pluginSetting = pluginSetting;
	}
	public Set<String> getFunctions() {
		return functions;
	}
	public void setFunctions(Set<String> functions) {
		this.functions = functions;
	}
	public Set<String> getFunctionImages() {
		return functionImages;
	}
	public void setFunctionImages(Set<String> functionImages) {
		this.functionImages = functionImages;
	}
	public Set<String> getFunctionClasses() {
		return functionClasses;
	}
	public void setFunctionClasses(Set<String> functionClasses) {
		this.functionClasses = functionClasses;
	}
	public Set<String> getDatabases() {
		return databases;
	}
	public void setDatabases(Set<String> databases) {
		this.databases = databases;
	}
	public Set<String> getModules() {
		return modules;
	}
	public void setModules(Set<String> modules) {
		this.modules = modules;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public boolean isCreateLog() {
		return isCreateLog;
	}
	public void setCreateLog(boolean isCreateLog) {
		this.isCreateLog = isCreateLog;
	}

	public String getFunctionIsOpens() {
		return functionIsOpens;
	}

	public void setFunctionIsOpens(String functionIsOpens) {
		this.functionIsOpens = functionIsOpens;
	}
	
	
	
}
