package com.gpluslife.home.plugin;

import java.util.ArrayList;

import com.gpluslife.R;
import com.gpluslife.customView.GPNavigationView;
import com.gpluslife.home.plugin.adapter.GPDeviceManagementAdapter;
import com.gpluslife.home.plugin.model.GPPluginModel;
import com.gpluslife.utils.GPConstants;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;

//import com.GetDeviceCommon;
//import com.GrobalConstance;
//import com.gpluslife.adapter.DeviceManagerAdapter;
//import com.gpluslife.adapter.DeviceManagerAdapter.GplusDevice;
//import com.gpluslife.service.MoreUserLoginMsg;
//import com.gpluslife.utils.PreferencesUtils;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint({ "NewApi", "ShowToast" })
public class GPDeviceMangementActivity extends Activity implements OnClickListener 
 {

	 //连接成功
	public final static String ACTION_GATT_CONNECTED = "com.gplus.blacelet.broadcast.ACTION_GATT_CONNECTED";
	private ExpandableListView mExpListView;
	
	private GPNavigationView mNavigationView;
	private Button	mBackButton;
	private Button mAddButton;
	
	public static final int REQUEST_SELECT_DEVICE = 1;
	public static final int REQUEST_ENABLE_BT = 2;
	private ArrayList<GPPluginModel> pluginModels;
	private ArrayList<ArrayList<View>> mDeviceFuctions;
	private GPDeviceManagementAdapter adapter;
	Boolean boolean1=false;  
	
	//注册监听，连接是返回true，未连接默认false
//	final BroadcastReceiver mReceiver = new BroadcastReceiver() {
//		@Override
//		public void onReceive(Context arg0, Intent intent) {
//			// TODO Auto-generated method stub
//			String action = intent.getAction();
//			if (action.equals(ACTION_GATT_CONNECTED)) {
//				boolean1=true;
//				Log.d("onReceive", boolean1+"");
//				updateboolean1Statue(boolean1);
//			}
//		}
//	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_device_manage);
		
		//过滤广播
		//==============================================
//		IntentFilter mFilter = new IntentFilter();
//		mFilter.addAction(ACTION_GATT_CONNECTED);
//		registerReceiver(mReceiver, mFilter);
		//==============================================
	
		pluginModels = new  ArrayList<GPPluginModel> ();
		readSelectedPluginFromLocal();
		
		mNavigationView = (GPNavigationView)findViewById(R.id.navigationView);
		mBackButton = mNavigationView.getBtn_left();
		mAddButton = mNavigationView.getBtn_right();
		mBackButton.setOnClickListener(this);
		mAddButton.setOnClickListener(this);
		
		adapter = new GPDeviceManagementAdapter(GPDeviceMangementActivity.this);
		adapter.setModels(pluginModels);		
		mDeviceFuctions = new ArrayList<ArrayList<View>>();
		mExpListView=(ExpandableListView) findViewById(android.R.id.list);
		mExpListView.setAdapter(adapter);
		
		
		
		//长按设备绑定、解绑。根据boole值状态选择解绑或绑定dialog
		
		mExpListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				if(boolean1==false){
				    showInfo1();
				}else if(boolean1 ==true){
					showInfo2();
				}
				return true;
			}
		});
		
	}
	
	    //绑定
		public void showInfo1() 
		{
			new AlertDialog.Builder(this)
					.setTitle("绑定")
					.setNeutralButton("取消", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
						}
					})
					.setPositiveButton("绑定", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int whichButton) 
						{
						
							Intent  SearchIntent = new Intent("com.gplus.blacelet.search");
		                    SearchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        			startActivity(SearchIntent);
						}
					})
					.show();
		}
		//解绑
		public void showInfo2() 
		{
			new AlertDialog.Builder(this)
					.setTitle("解绑")
					.setNeutralButton("取消", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
						}
					})
					.setPositiveButton("解绑", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int whichButton) 
						{
							
							Intent  SearchIntent = new Intent("com.glife.jordan.engine.closeBtConection()");
		                    SearchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        			startActivity(SearchIntent);
						}
					})
					.show();
		}
		
	
		
	public Boolean updateboolean1Statue(Boolean booleString) {
			boolean1=booleString;
			return boolean1;
	}
	@Override
	protected void onResume() {
		super.onResume();
		initFunctions();
//		MoreUserLoginMsg moreUserLoginMsg =new MoreUserLoginMsg(this);
//		moreUserLoginMsg.Dialog();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
//		unregisterReceiver(mReceiver);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	private void initFunctions () {
//		mDevices.clear();
//		mDeviceFuctions.clear();
//		addDeviceBlacelet();
//		if(mDevices.size() < 1|| mDeviceFuctions.size() <1 ){
//			setListAdapter(null);
//			mExpListView.invalidate();
//			return;
//		}
//		adapter = new DeviceManagerAdapter(this, mDevices, mDeviceFuctions);
//		setListAdapter(adapter);
//		mExpListView.expandGroup(0);
//		adapter.setListValues(mDevices, mDeviceFuctions);
//		mExpListView.invalidate();
	}

	private void addDeviceBlacelet(){
//		if(!PreferencesUtils.getBoolean(this, GrobalConstance.GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED)){
			return;
//		}
//		mDevices.add(new GplusDevice(R.string.device_balcelet, R.drawable.manage_jordan_icon));
//		ArrayList<View> blaceLet = new ArrayList<View>();
//		Context targetContext;
//		try {
//			targetContext = GetDeviceCommon.getTargetContext(this, GrobalConstance.GROBAL_PREFERENCE_KEY_DEVICE_BLACELET_ADDED);
//			View  v =GetDeviceCommon.getView(targetContext, 
//					GetDeviceCommon.getId(targetContext, "layout", "settings_blacelet"));
//			blaceLet.add(v);
//		} catch (NameNotFoundException e) {
//			e.printStackTrace();
//		}
//		blaceLet.add(new SettingsStepsView(this));
//		blaceLet.add(new SettingsDistanceView(this));
//		blaceLet.add(new SettingsCaloriView(this));
//		blaceLet.add(new SettingsSleepView(this));
//		blaceLet.add(new SettingsStairView(this));
//		blaceLet.add(new SettingsClockView(this));
//		mDeviceFuctions.add(blaceLet);
	}
	@Override
	public void onClick(View V) {
		switch (V.getId()) {

		case GPConstants.NAVIGATION_LEFT_BUTTON:
		{
			onBackPressed();
			finish();
		}
			break;
		case GPConstants.NAVIGATION_RIGHT_BUTTON:
			Intent intent2=new Intent();
			intent2.setClass(this, GPAddPluginActivity.class);
			startActivity(intent2);
			finish();
		default:
			break;
		}
	}
	
	private void readSelectedPluginFromLocal()
	{
	 	SharedPreferences sp = this.getSharedPreferences("1000", Context.MODE_PRIVATE);
	 	if (sp.getBoolean("isSelected",false) == true) 
	 	{
		 	GPPluginModel model = new GPPluginModel();
		 	model.setPluginId(sp.getString("pluginId", null));
		 	model.setPluginName(sp.getString("pluginName", null));
		 	model.setPluginSetting(sp.getString("pluginSetting", null));
		 	model.setPluginThumb(sp.getString("pluginThumb", null));
		 	model.setFunctionIsOpens(sp.getString("functionIsOpen", null));
		 	model.setFunctions(sp.getStringSet("function", null));
		 	model.setFunctionClasses(sp.getStringSet("functionClass", null));
		 	model.setFunctionImages(sp.getStringSet("functionImage", null));
		 	model.setCreateLog(sp.getBoolean("isCreateLog", false));
		 	model.setSelected(sp.getBoolean("isSelected",false));
		 	pluginModels.add(model);
		}

	 	
	 	SharedPreferences sp2 = this.getSharedPreferences("1002", Context.MODE_PRIVATE);
	 	if (sp2.getBoolean("isSelected",false) == true) 
	 	{
		 	GPPluginModel model2 = new GPPluginModel();
		 	model2.setPluginId(sp2.getString("pluginId", null));
		 	model2.setPluginName(sp2.getString("pluginName", null));
		 	model2.setPluginSetting(sp2.getString("pluginSetting", null));
		 	model2.setPluginThumb(sp2.getString("pluginThumb", null));
		 	model2.setFunctionIsOpens(sp2.getString("functionIsOpen", null));
		 	model2.setFunctions(sp2.getStringSet("function", null));
		 	model2.setFunctionClasses(sp2.getStringSet("functionClass", null));
		 	model2.setFunctionImages(sp2.getStringSet("functionImage", null));
		 	model2.setCreateLog(sp.getBoolean("isCreateLog", false));
		 	model2.setSelected(sp.getBoolean("isSelected",false));
		 	pluginModels.add(model2);
	 	}
	}
	
}